#include <string>
using namespace std;

#ifndef CONST_H
#define CONST_H

const string objetivoDeVoo[2]={
    "Decolar",
    "Pousar"
};

const string tiposDeEmergencia[4]{
    "Não",
    "Sim - Excedeu o tempo de espera máximo",
    "Sim",
    "Sim - Sem combustível"
};

const string companhias[5]={
    "AD",
    "JJ",
    "G3",
    "W8",
    "CO"
};

const string aeroportos[30]={
    "BSB",
    "VIX",
    "GIG",
    "SSA",
    "FLN",
    "POA",
    "NVT",
    "REC",
    "CWB",
    "BEL",
    "UDI",
    "JOI",
    "CGB",
    "CGR",
    "FOR",
    "MCP",
    "MGF",
    "GYN",
    "MAO",
    "NAT",
    "BPS",
    "MCZ",
    "PMW",
    "SLZ",
    "LDB",
    "PVH",
    "RBR",
    "THE",
    "AJU",
    "CXJ"
};

const int duracaoVoo[30]{
    94,
    87,
    58,
    138,
    67,
    95,
    63,
    188,
    57,
    215,
    70,
    65,
    135,
    98,
    205,
    214,
    80,
    91,
    235,
    202,
    112,
    173,
    140,
    205,
    75,
    220,
    235,
    190,
    155,
    95
};

#endif