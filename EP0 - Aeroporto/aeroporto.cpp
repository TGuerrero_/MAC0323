#include "aeroporto.h"
#include "const.h"

void aviao::inicializaAviao(){
    //Cria um avião com dados gerados aleatoriamente
    string n1, n2, n3;
    int aux;
    n1 = rand()%10 + 48; /*48 = '0' na tabela ASC II*/
    n2 = rand()%10 + 48;
    n3 = rand()%10 + 48;
    nome = companhias[rand()%5] + n1 + n2 + n3;

    aux = rand()%30;
    aeroporto = aeroportos[aux];
    objetivo = rand()%2;
    if (objetivo == 0){
        tempoDeVoo = duracaoVoo[aux];
        tempoDeEsperaMax = (tempoDeVoo * 0.1);
    }
    else
        combustivel = (rand()%60)+3;

    emergencia = rand()%10; /*10% de chance de ocorrer uma emergência*/
    if (emergencia != 2)
        emergencia = 0;
}

void aviao::inicializaAviaoManual(){
    //Cria, através de dados da entrada padrão, um avião
    cout<<"#########################";
    cout<<"\nGERAÇÃO DE VOO MANUAL";
    //cout<<"\nDigite o nome do voo: ";
    cin>>nome;
    //cout<<"Digite a sigla do aeroporto: ";
    cin>>aeroporto;
    //cout<<"Digite o objetivo do voo(0-Decolar || 1-Pousar): ";
    cin>>objetivo;
    //cout<<"Digite a emergência do voo(0-Normal || 2-Emergencial): ";
    cin>>emergencia;
    if (emergencia)         //Aplica a prioridade certa para os inputs de emergência
        emergencia = 2;
    if (objetivo == 1){
        //cout<<"Digite o combustível restante: ";
        cin>>combustivel;
    }
    else{
        //cout<<"Digite o tempo de voo: ";
        cin>>tempoDeVoo;
        tempoDeEsperaMax = (tempoDeVoo * 0.1);
    }
    cout<<"#########################\n";
}

void aviao::mostraAviao(){
    cout<<"\nNome do voo: "<<nome<<" |Aeroporto: "<<aeroporto<<" |Objetivo: "<<objetivoDeVoo[objetivo];
    if (objetivo == 0)
        cout<<" |Tempo de voo: "<<tempoDeVoo;
    else
        cout<<" |Combustível restante: "<<combustivel;
    cout<< " |Emergência: "<<tiposDeEmergencia[emergencia];
}

int aviao::mostraObjetivo(){
    return objetivo;
}

int aviao::mostraEmergencia(){
    return emergencia;
}

int aviao::mostraCombustivel(){
    return combustivel;
}

int aviao::mostraTempoDeEspera(){
    return tempoDeEsperaMax;
}

string aviao::mostraNome(){
    return nome;
}

void aviao::setaEmergencia(int novaEmergencia){
    emergencia = novaEmergencia;
}

void aviao::consomeCombustivel(){
    //A cada iteração diminui o combustível do avião
    if (mostraObjetivo())
        combustivel--;
}

void aviao::diminuiTempoDeEspera(){
    //A cada iteração diminui o tempo de espera máximo para decolagem do avião
    if (tempoDeEsperaMax > 0 && !mostraObjetivo())
        tempoDeEsperaMax--;
}

//###################################################################################################################

void pista::aplicaCooldown(int pista){
    //Aplica o tempo de espera para reuso da pista após um uso
    if (pista == 1)
        esperaPista1 = 3; /*'3' pois o valor é decrementado na própria iteração que é chamado*/
    else if (pista == 2)
        esperaPista2 = 3;
    else
        esperaPista3 = 3;
}

void pista::reduzCooldown(){
    //Diminui o tempo de espera para o reuso da pista
    if (esperaPista1 > 0)
        esperaPista1--;
    if (esperaPista2 > 0)
        esperaPista2--;
    if (esperaPista3 > 0)
        esperaPista3--;
}

bool pista::pistaLivre(int pista){
    if (pista == 1)
        return (esperaPista1 == 0);
    else if (pista == 2)
        return (esperaPista2 == 0);
    else
        return (esperaPista3 == 0);
}

void pista::usarPista(int pista, aviao *atual){
    cout<<"\nA pista "<<pista<<" foi usada para "<<objetivoDeVoo[atual->mostraObjetivo()]<<" pelo voo ";
    if (atual->mostraEmergencia())
        cout<<"emergencial ";
    if(atual->mostraEmergencia() == 1)
        cout<<"que excedeu o limite de espera ";
    else if (atual->mostraEmergencia() == 3)
        cout<<"sem combustível ";
    cout<<atual->mostraNome();
    aplicaCooldown(pista);
}

void pista::escolhePista(aviao *atual){
    //Recebe um voo e decide para qual pista será direcionado o voo
    if (atual->mostraObjetivo() == 0){
        if (pistaLivre(3))
            usarPista(3, atual);
        else if(pistaLivre(1))
            usarPista(1, atual);
        else if(pistaLivre(2))
            usarPista(2, atual);
    }
    else{
        if(pistaLivre(1))
            usarPista(1, atual);
        else if(pistaLivre(2))
            usarPista(2, atual);
        else if (pistaLivre(3) && atual->mostraEmergencia())
            usarPista(3, atual);
    }
    delete atual;
}

int pista::maiorTempoDeCoolDown(){
    /*Retorna a pista que tem o maior tempo de cooldown*/
    if (esperaPista1 > esperaPista2){
        if (esperaPista1 > esperaPista3)
            return esperaPista1;
    }
    else if (esperaPista2 > esperaPista3)
        return esperaPista2;
    else
        return esperaPista3;
}