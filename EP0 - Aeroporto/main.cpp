#include "fila.h"
#include "aeroporto.h"
using namespace std;

int main(){
    int tempoTotal, tipoDeExecucao; /*Flags*/
    int ligacoes, pousosPorIteracao, combustivelPorIteracao;
    fila listaDeEspera;
    pista aeroporto;
    aviao *vooAtual;
    cel * aux;

    cout << "#################################################\n";
    cout << "    Sistema de Gerenciamento de  aeroportos\n";
    cout << "#################################################\n";

    cout << "\nDigite o tempo de simulação(em minutos): ";
    cin >> tempoTotal;
    cout << "\nDeseja simular manualmente os voos(1-Sim, 0-Nao): ";
    cin >> tipoDeExecucao; 

    srand(2020);
    for(int i=0; tempoTotal > 0; i++){
        pousosPorIteracao = combustivelPorIteracao = 0;

        cout<<"\nITERAÇÃO "<<i;
        if (tipoDeExecucao == 0)
            ligacoes = rand()%3;
        else{
            cout <<"\nDigite o número de ligações dessa iteração: ";
            cin >> ligacoes;
        }
        cout<<"\nAviões que entraram em contato com a torre: "<<ligacoes<<"\n";
        while (ligacoes > 0){
            aux = criaCelula(tipoDeExecucao);
            if (aux->aviaoAtual()->mostraEmergencia()){
                listaDeEspera.insereNaFilaComPrioridade(aux);
                if (aux->aviaoAtual()->mostraObjetivo() && aux->aviaoAtual()->mostraCombustivel() <= 2){
                    //Checa se é uma emergência por falta de combustível
                    aux->aviaoAtual()->setaEmergencia(3);
                    listaDeEspera.incrementaNumDeEmergencias();
                    if (listaDeEspera.inicioDaFila() != aux->aviaoAtual()){
                        if (!listaDeEspera.podePousar(aux, aeroporto)){
                            listaDeEspera.desviaVoo(aux);
                            listaDeEspera.decrementaNumDeEmergencias();
                        }
                        else
                            listaDeEspera.reinsereComPrioridade(aux);
                    }
                }
            }
            else
                listaDeEspera.insereNaFila(aux);
            ligacoes--;
        }

        listaDeEspera.mostraFila();
        while (!listaDeEspera.filaVazia() && (aeroporto.pistaLivre(1) || aeroporto.pistaLivre(2) || aeroporto.pistaLivre(3))){
            
            if (!aeroporto.pistaLivre(1) && !aeroporto.pistaLivre(2) && aeroporto.pistaLivre(3) &&
             !listaDeEspera.inicioDaFila()->mostraEmergencia() && listaDeEspera.inicioDaFila()->mostraObjetivo()){
                //Checa se existe alguma decolagem na fila para evitar deixar a pista 3 ociosa
                vooAtual = listaDeEspera.removeDecolagemDaFila();
                if (vooAtual == nullptr)
                    break;
            }
            else
                vooAtual = listaDeEspera.removeDaFila();

            if (vooAtual->mostraObjetivo()){
                pousosPorIteracao++;
                combustivelPorIteracao += vooAtual->mostraCombustivel();
            }
            aeroporto.escolhePista(vooAtual);
        }

        aeroporto.reduzCooldown();
        listaDeEspera.atualizaSituacaoFila(aeroporto);
        cout<<"\nA quantidade média de combustível dos aviões que pousaram é: ";
        if (pousosPorIteracao != 0)
            cout<<(double)combustivelPorIteracao/pousosPorIteracao;
        else
            cout<< 0;
        tempoTotal--;
        cout<<"\n";
    }

    return EXIT_SUCCESS;
}