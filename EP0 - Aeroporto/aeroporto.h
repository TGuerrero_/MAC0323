#include <iostream>
using namespace std;

#ifndef AEROPORTO_H
#define AEROPORTO_H


class aviao{
    private:
        string nome;
        string aeroporto;
        int objetivo;           /*0 - Decolagem | 1 - Aterrisagem */
        int tempoDeVoo;         /*Para decolagem*/
        int tempoDeEsperaMax;   /*Para decolagem*/
        int combustivel;        /*Para pouso*/
        int emergencia;         /*0 - Voo Normal | 1 - Voo excedeu 10% do tempo de espera | 2 - Voo Emergencial | 3 - Voo sem combustível*/
    public:
        aviao():tempoDeVoo(0), tempoDeEsperaMax(0), combustivel(0) {};
        void inicializaAviao();
        void inicializaAviaoManual();
        void mostraAviao();
        int mostraObjetivo();
        int mostraEmergencia();
        int mostraCombustivel();
        int mostraTempoDeEspera();
        string mostraNome();
        void setaEmergencia(int novaEmergencia);
        void consomeCombustivel();
        void diminuiTempoDeEspera();
};

class pista{
    private:
        int esperaPista1, esperaPista2, esperaPista3;
        void aplicaCooldown(int pista);
        void usarPista(int pista, aviao *atual);
    public:
        pista(): esperaPista1(0), esperaPista2(0), esperaPista3(0){};
        void reduzCooldown();
        bool pistaLivre(int pista);
        void escolhePista(aviao *atual);
        int maiorTempoDeCoolDown();
};
#endif