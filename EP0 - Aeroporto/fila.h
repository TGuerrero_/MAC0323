#include "aeroporto.h"
using namespace std;
#ifndef FILA_H
#define FILA_H


class cel{
    private:
        aviao *atual;
        cel * prox;
        cel * ant;
    public:
        cel();
        ~cel();
        void setaAviao(aviao *obj);
        void setaProx(cel *obj);
        void setaAnt(cel *obj);
        aviao *aviaoAtual();
        cel *proximaCelula();
        cel *celulaAnterior();
};

class fila{
    private:
        cel *ini, *fim;
        int numDeEmergencias; //Conta a quantidade de emergências de maior prioridade
        int tamFila;
    public:
        fila();
        ~fila();
        void insereNaFilaComPrioridade(cel *obj);
        void insereNaFila(cel *obj);
        void reinsereComPrioridade(cel * obj);
        aviao *removeDaFila();
        aviao *removeDecolagemDaFila();
        void excluiDaFila(cel *obj);
        aviao *inicioDaFila();
        bool filaVazia();
        void mostraFila();
        void atualizaSituacaoFila(pista aeroporto);
        bool podePousar(cel *obj, pista aeroporto);
        void desviaVoo(cel *obj);
        void incrementaNumDeEmergencias();
        void decrementaNumDeEmergencias();
};

/*## Funções sem classe ##*/
cel *criaCelula(int execucao);

aviao *criaAviao(int execucao);
#endif