#include "fila.h"

cel::cel():atual(nullptr), prox(nullptr), ant(nullptr) {};

cel::~cel(){
    if (atual != nullptr)
        delete atual;
}

void cel::setaAviao(aviao *obj){
    atual = obj;
}

void cel::setaProx(cel *obj){
    prox = obj;
}

void cel::setaAnt(cel *obj){
    ant = obj;
}

aviao *cel::aviaoAtual(){
    return atual;
}

cel *cel::proximaCelula(){
    return prox;
}

cel *cel::celulaAnterior(){
    return ant;
}

//###################################################################################################################

fila::fila(): ini(nullptr), fim(nullptr), numDeEmergencias(0), tamFila(0) {};

fila::~fila(){
    cel *aux;
    if (!filaVazia())
        while (ini != nullptr){
            aux = ini;
            ini = ini->proximaCelula();
            delete aux;
        }
}

void fila::insereNaFilaComPrioridade(cel *obj){
    //Insere a célula na fila com prioridade, ou seja, na frente de todos os voos não emergenciais
    cel * aux=ini;
    bool insereNoInicio = true;
    if (!filaVazia()){
        while (aux !=nullptr && aux->aviaoAtual()->mostraEmergencia() >= obj->aviaoAtual()->mostraEmergencia()){
            aux = aux->proximaCelula();
            insereNoInicio = false;
        }
        if (aux == nullptr){
            insereNaFila(obj);
            return;
        }

        else if (insereNoInicio){
            obj->setaProx(ini);
            obj->setaAnt(nullptr);
            ini->setaAnt(obj);
            ini = obj;
        }
        else{
            aux->celulaAnterior()->setaProx(obj);
            obj->setaAnt(aux->celulaAnterior());
            aux->setaAnt(obj);
            obj->setaProx(aux);
        }
    }
    else
        ini = fim = obj;
    tamFila++;
}

void fila::insereNaFila(cel *obj){
    if (!filaVazia()){
        fim->setaProx(obj);
        obj->setaAnt(fim);
        fim = obj;
    }
    else
        ini = fim = obj;
    tamFila++;
}

void fila::reinsereComPrioridade(cel * obj){
    /*Reinsere a célula na fila com prioridade, ou seja, na frente de todos os voos com prioridade menor que o voo atual*/
    excluiDaFila(obj);
    insereNaFilaComPrioridade(obj);
}

aviao *fila::removeDaFila(){
    /*Retira da fila o avião que está no início da fila*/
    aviao *ret;
    cel * aux;
    if (!filaVazia()){
        ret = ini->aviaoAtual();
        aux = ini;
        ini = ini->proximaCelula();
        aux->setaAviao(nullptr);
        if (!filaVazia())
            ini->setaAnt(nullptr);
        delete aux;
        tamFila--;
        if (ret->mostraEmergencia() == 3)
            numDeEmergencias--;
        return ret;
    }
    return nullptr;
}

aviao *fila::removeDecolagemDaFila(){
    /*Percorre a fila procurando o primeiro avião querendo decolar.
    Obs: Assumesse que a fila não possui nenhuma emergência*/
    cel *aux=ini;
    aviao *ret;
    while (aux != nullptr && aux->aviaoAtual()->mostraObjetivo()){
        aux = aux->proximaCelula();
    }
    if (aux != nullptr){
        ret = aux->aviaoAtual();
        excluiDaFila(aux);
        aux->setaAviao(nullptr);
        delete aux;
        return ret;
    }
    return nullptr;
}

void fila::excluiDaFila(cel *obj){
    /*Exclui a célula da fila em qualquer posição*/
    if (obj->celulaAnterior() != nullptr)
        obj->celulaAnterior()->setaProx(obj->proximaCelula());
    if (obj->proximaCelula() != nullptr)
        obj->proximaCelula()->setaAnt(obj->celulaAnterior());
    if (ini == obj && fim == obj){
        ini = nullptr;
        fim = nullptr;
    }
    else if (ini == obj)
        ini = ini->proximaCelula();
    else if (fim == obj)
        fim = obj->celulaAnterior();
    tamFila--;
}

aviao *fila::inicioDaFila(){
    return ini->aviaoAtual();
}

bool fila::filaVazia(){
    return (ini == nullptr);
}

void fila::mostraFila(){
    cel * aux = ini;
    cout<<"\n## Voos na lista de espera ##";
    if (filaVazia()){
        cout << "\nFila vazia";
    }
    else
        while (aux != nullptr){
            aux->aviaoAtual()->mostraAviao();
            aux = aux->proximaCelula();
        }
    cout<<"\n";
}

void fila::atualizaSituacaoFila(pista aeroporto){
    //Percorre a fila fazendo as alterações e cálculos necessárias(os).
    cel * aux=ini, *aux2;
    int numPousos=0, numDecolagens=0;
    int qtdCombustivel=0; 
    bool reinsere, desvia;


    if (!filaVazia()){
        while(aux != nullptr){
            reinsere = false;
            desvia = false;
            aux2 = aux;
            aux = aux->proximaCelula();

            if (aux2->aviaoAtual()->mostraObjetivo()){
                numPousos++;
                qtdCombustivel += aux2->aviaoAtual()->mostraCombustivel();
                aux2->aviaoAtual()->consomeCombustivel();
                if (aux2->aviaoAtual()->mostraCombustivel() <= 3 && aux2->aviaoAtual()->mostraEmergencia() != 3){
                    //Sem combustível
                    aux2->aviaoAtual()->setaEmergencia(3);
                    if (inicioDaFila() != aux2->aviaoAtual())
                        desvia = !podePousar(aux2, aeroporto);
                    if (desvia)
                        desviaVoo(aux2);
                    else{
                        reinsere = true;
                        numDeEmergencias++;
                    }
                }
            }
            else if (!aux2->aviaoAtual()->mostraObjetivo()){
                numDecolagens++;
                aux2->aviaoAtual()->diminuiTempoDeEspera();
                if (aux2->aviaoAtual()->mostraTempoDeEspera() == 0 && aux2->aviaoAtual()->mostraEmergencia() != 1){
                    //Excedeu o tempo de espera
                    aux2->aviaoAtual()->setaEmergencia(1);
                    reinsere = true;
                }
            }
            if (ini != aux2 && reinsere)
                reinsereComPrioridade(aux2);
        }
    }   
    cout<<"\nO tempo médio de espera para pouso é: ";
    if (!filaVazia())
        cout<<((double)numPousos/tamFila)*(2/3) + aeroporto.maiorTempoDeCoolDown() + ((double)numPousos/tamFila)/3;
    else
        cout<<0;
    cout<<"\nO tempo médio de espera para decolagem é: ";
    if (!filaVazia())
        cout<<((double)numDecolagens/tamFila)*(2/3) + aeroporto.maiorTempoDeCoolDown() + ((double)numDecolagens/tamFila)/3;
    else
        cout<<0;
    cout<<"\nA quantidade média de combustível dos aviões esperando para pouso é: ";
    if (numPousos != 0)
        cout<<(double)qtdCombustivel/numPousos;
    else
        cout<<0;
}

bool fila::podePousar(cel *obj, pista aeroporto){
    /*Verifica se, um voo que acabou de virar emergencia por falta de combustível,
    consegue pousar ou não com o combustível restante.
    True - Consegue pousar;
    False - Não consegue (precisa ser desviado para outro aeroporto).
    */
   int tempoDeEsperaMax=0, cooldownMax=0;
   cooldownMax = (numDeEmergencias*2/3) + aeroporto.maiorTempoDeCoolDown();
   tempoDeEsperaMax = cooldownMax + (numDeEmergencias/3);
   return (obj->aviaoAtual()->mostraCombustivel() - tempoDeEsperaMax >= 0);
}

void fila::desviaVoo(cel *obj){
    excluiDaFila(obj);
    cout<<"\nO voo "<<obj->aviaoAtual()->mostraNome()<<" foi desviado para um aeroporto vizinho devido a uma situação crítica de falta de combustível";
    delete obj;
}

void fila::incrementaNumDeEmergencias(){
    numDeEmergencias++;
}

void fila::decrementaNumDeEmergencias(){
    numDeEmergencias--;
}

/*## Funções sem classe ##*/
cel *criaCelula(int execucao){
    /*Recebe o tipo de execução(manual ou automático) e cria uma célula*/
    cel * ret;
    aviao *atual;

    ret = new(cel);
    atual = criaAviao(execucao);
    ret->setaAviao(atual);
    return ret;
}

aviao *criaAviao(int execucao){
    /*Recebe o tipo de execução(manual ou automático) e cria um avião*/
    aviao * ret;

    ret = new(aviao);
    if (execucao == 1)
        ret->inicializaAviaoManual();
    else if (execucao == 0) 
        ret->inicializaAviao();
    return ret;
}
