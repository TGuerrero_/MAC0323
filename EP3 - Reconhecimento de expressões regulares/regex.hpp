#include <iostream>
using namespace std;

#ifndef REGEX_H
#define REGEX_H

/*
* Transforma abreviaturas em uma regex entendível pelo autômato
* Ex: "BANA(NA)+" vira "BANANA(NA)*"
* Ex2: "[AB]" vira "(A|B)"
*/
string regexManipulation(string regex);


/*
* Transforma intervalos de uma regex em um conjunto
* Ex: "[A-D]"" vira "[ABCD]"
*/
string rangeManipulation(string regex);

/*
* Transforma as operções nas operações triviais
* Ex: "BANA(NA)+"" vira "BANANA(NA)*"
* Obs: É esperado receber uma regex sem intervalos
*/
string operataionsManipulation(string regex);

#endif