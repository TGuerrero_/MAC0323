#include "grafo.hpp"
#include "regex.hpp"
using namespace std;

#define PROMPT cout<<">>> "
/*-----------------------------------------------------------*/
/* Declaração das funções */
/*
* Executa uma interface de testes iterativo que recebe uma palavra
* e printa se ela faz parte da linguagem da regex ou não
* A função é finalizada após receber um CTRL+D
*/
void interface(grafo transitions, string regex);

/*
* Percorre o autômato e retorna true se a palavra faz parte da linguagem reconhecida pela regex
*/
bool wordMatch(grafo transitions, string regex, string word);

int main (){
    string regex, word;
    cout<<"Digite a regex: ";
    cin>>regex;
    //Pré Processamento
    regex = regexManipulation(regex);
    grafo transitions(regex);
    interface(transitions, regex);
    return 0;
}

void interface(grafo transitions, string regex){
    string word;
    PROMPT;
    while (cin>>word){
        cout<<boolalpha<<wordMatch(transitions, regex, word)<<endl;
        PROMPT;
    }
}

bool wordMatch(grafo transitions, string regex, string word){
    vector<bool>marked(regex.length()+1, false);
    stack<int>reached;
    transitions.dfs(0, marked);
    for (int i=0; i < (int)word.length(); i++){
        for (int j=0; j < (int)marked.size()-1; j++){
            if (marked[j] && (regex[j] == word[i] || ((j-1 < 0 || regex[j-1] != '\\')&& regex[j] == '.')) ){
                //Leio e consigo chegar na próxima posição
                reached.push(j+1);
            }
        }
        for (int k=0; k < (int)marked.size(); k++){
            marked[k] = false;
        }
        while (!reached.empty()){
            transitions.dfs(reached.top(), marked);
            reached.pop();
        }
    }
    //marked[regex.length()] == Estado de aceitação
    return marked[regex.length()];
}