#include "regex.hpp"
#include <stack>
#include <vector>
using namespace std;

string regexManipulation(string regex){
    regex = rangeManipulation(regex);
    regex = operataionsManipulation(regex);
    return regex;
}

string rangeManipulation(string regex){
    for (int i=0; i < (int)regex.length(); i++){
        if (regex[i] == '\\'){
            //Pula o próximo caractere (Caractere especial usado como normal)
            i++;
        }
        
        else if (regex[i] == '-'){
            //Intervalo
            int k=0;
            char begin = regex[i-1], end = regex[i+1]-1;
            regex.erase(i, 1);
            for (char var=end; var > begin; var--, k++){
                string current;
                current += var;
                regex.insert(i, current);
            }
            i += k;
        }
    }
    return regex;
}

string operataionsManipulation(string regex){
    stack<int>symbols;
    for (int i=0; i < (int)regex.length(); i++){
        if (!symbols.empty() && regex[symbols.top()] == '['){
            //Conjunto
            if (i-1 != symbols.top() && regex[i] != ']'){
                regex.insert(i, "|");
                i++;
            }
        }
        if (regex[i] == '\\'){
            //Pula o próximo caractere (Caractere especial usado como normal)
            i++;
        }
        else if (regex[i] == '(' || regex[i] == '[')
            symbols.push(i);

        else if (regex[i] == ')'){
            if (i < (int)regex.length()-1 && regex[i+1] != '+')
                symbols.pop();
        }
        else if (regex[i] == ']'){
            //Fim do conjunto
            regex.replace(symbols.top(), 1, "(");
            regex.replace(i, 1, ")");
            if (i < (int)regex.length()-1 && regex[i+1] != '+')
                symbols.pop();
        }
        else if (regex[i] == '+'){
            //Um ou mais
            regex.replace(i, 1, "*");
            if (regex[i-1] == ')'){
                int subtrLen = i-2-symbols.top();
                string subtr = "(";
                subtr += regex.substr(symbols.top()+1, subtrLen);
                subtr += ")";
                regex.insert(symbols.top(), subtr);
                i += subtrLen+2;
                symbols.pop();
            }
            else
                regex.insert(i, regex.substr(i-1, 1));
        }
        else if (regex[i] == '^' && !symbols.empty() && regex[symbols.top()] == '['){
            //Complemento
            int j;
            vector<bool>ascII(96, true);
            string replace;

            for (j=i+1; regex[j] != ']'; j++){
                ascII[regex[j]-32] = false;
            }

            for (int k=0; k < (int)ascII.size(); k++){
                if (ascII[k]){
                    if ((k >= (40-32) && k <= (46-32)) || (k >= (91-32) && k <= (94-32)) || k == (124-32))
                        replace += "\\";
                    char current = k + 32;
                    replace += current;
                }
            }
            regex.erase(i, j - i);
            regex.insert(i, replace);
        }
    }
    return regex;
}