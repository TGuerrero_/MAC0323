#include "grafo.hpp"
using namespace std;

grafo::grafo(string regex){
    vertexList = vector<vector<int>>(regex.length()+1); //+1 == Estado de aceitação
    insertTransitionsEdges(regex);
}

void grafo::insertEdge(int to, int from){
    vertexList[to].push_back(from);
}

void grafo::insertTransitionsEdges(string regex){
    stack<int>symbols;
    stack<int>parenthesis;
    for (int i=0; i < (int)regex.length(); i++){
        int ant = i;

        if (regex[i] == '\\'){
            //Indenpendente de qual caractere for o regex[i+1], será tratado como uma letra normal
            insertEdge(i, i+1);
            i++;
        }

        else if (regex[i] == '(' || regex[i] == '|'){
            if (regex[i] == '(')
                parenthesis.push(i);
            symbols.push(i);
        }

        else if (regex[i] == ')'){
            while (regex[symbols.top()] != '('){
                int top = symbols.top();
                symbols.pop();
                if (regex[top] == '|'){
                    insertEdge(top, i);
                    insertEdge(parenthesis.top(), top+1);
                }
            }
            ant = symbols.top();
            symbols.pop();
            parenthesis.pop();
        }

        if (i < (int)regex.length()-1 && regex[i+1] == '*'){
            insertEdge(ant, i+1);
            insertEdge(i+1, ant);
        }

        if (regex[i] == '(' || regex[i] == ')' || regex[i] == '*')
            insertEdge(i, i+1);
    }
}

void grafo::dfs(int vertex, vector<bool>&marked){
    marked[vertex] = true;
    for (int i=0; i < (int)vertexList[vertex].size(); i++){
        if (!marked[vertexList[vertex][i]])
            dfs(vertexList[vertex][i], marked);
    }
}

void grafo::print(){
    for (int i=0; i < (int)vertexList.size(); i++){
        cout<<i;
        for (int j=0; j < (int)vertexList[i].size(); j++){
            cout<<" -> "<<vertexList[i][j];
        }
        cout<<endl;
    }
}