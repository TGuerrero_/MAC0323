#include <iostream>
#include <vector>
#include <stack>
using namespace std;

#ifndef GRAFO_H
#define GRAFO_H

//Autômato
class grafo {
    private:
        vector<vector<int>>vertexList;
        /*
        Insere uma aresta de "to" para "from"
        */
        void insertEdge(int to, int from);
        /*
        * Recebe a regex do autômato e constroi todas as epsilon-transições
        */
        void insertTransitionsEdges(string regex);
    public:
        grafo(string regex);
        void dfs(int vertex, vector<bool>&marked);
        void print();
};
#endif