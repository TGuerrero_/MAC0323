#include "util.hpp"
using namespace std;

#ifndef VECTOR_H
#define VECTOR_H

//############################     Declaração dos Nós   ############################
    template <class Chave, class Item>
    class no_vector{
        private:
            Chave ch;
            Item val;
        public:
            Chave getChave();
            Item getValor();
            void setValor(Item novoValor);
            void setChave(Chave novaChave);
    };

    template <class Chave, class Item>
    Chave no_vector<Chave, Item>::getChave(){
        return ch;
    }

    template <class Chave, class Item>
    Item no_vector<Chave, Item>::getValor(){
        return val;
    }

    template <class Chave, class Item>
    void no_vector<Chave, Item>::setValor(Item novoValor){
        val = novoValor;
    }

    template <class Chave, class Item>
    void no_vector<Chave, Item>::setChave(Chave novaChave){
        ch = novaChave;
    }

//############################     Vetor Desordenado    ############################
    template <class Chave, class Item>
    class vetorDes{
        private:
            no_vector<Chave, Item> *vector;
            int n;
            int tamMax;
            void resize();
        public:
            vetorDes(string nomeDoTexto);
            ~vetorDes();
            void insere (Chave chave, Item valor);
            Item devolve (Chave chave);
            void remove (Chave chave);
            int rank (Chave chave); /*número de chaves < chave*/
            Chave seleciona(int k);
    };

    template <class Chave, class Item>
    vetorDes<Chave, Item>::vetorDes(string nomeDoTexto){
        vector = new no_vector<Chave, Item>[1];
        n = 0;
        tamMax = 1;

        fstream texto;
        string word;
        texto.open(nomeDoTexto);
        while (texto >> word){
            word = getOnlyWord(word);
            insere(word, 1);
        }
        texto.close();
    }

    template <class Chave, class Item>
    vetorDes<Chave, Item>::~vetorDes(){
        delete [] vector;
    }

    template <class Chave, class Item>
    void vetorDes<Chave, Item>::insere(Chave chave, Item valor){
        if (n == tamMax)
            resize();
        
        for (int i=0; i < n; i++){
            if (vector[i].getChave() == chave){
                vector[i].setValor(vector[i].getValor()+valor);
                return;
            }
        }
        vector[n].setChave(chave);
        vector[n].setValor(valor);
        n++;
    }

    template <class Chave, class Item>
    Item vetorDes<Chave, Item>::devolve(Chave chave){
        for (int i=0; i < n; i++){
            if (vector[i].getChave() == chave)
                return (vector[i].getValor());
        }
        return 0;
    }

    template <class Chave, class Item>
    void vetorDes<Chave, Item>::remove(Chave chave){
        int i, j;
        for (i=0; i < n && vector[i].getChave() != chave; i++);

        if (i < n){
            for (j=i; j < n-1; j++){
                vector[j] = vector[j+1];
            }
            n--;
        }
    }

    template <class Chave, class Item>
    int vetorDes<Chave, Item>::rank(Chave chave){
        int k=0;
        for (int i=0; i < n; i++){
            if (vector[i].getChave() < chave)
                k++;
        }
        return k;
    }

    template <class Chave, class Item>
    Chave vetorDes<Chave, Item>::seleciona(int k){
        for (int i = 0; i < n; i++){
            if (rank(vector[i].getChave()) == k)
                return (vector[i].getChave());
        }
        return "";
    }

    //Funções privadas

    template <class Chave, class Item>
    void vetorDes<Chave, Item>::resize(){
        no_vector<Chave, Item> *aux = new no_vector<Chave, Item>[2*tamMax];
        for (int i=0; i < n; i++)
            aux[i] = vector[i];
        delete [] vector;
        vector = aux;
        tamMax = 2*tamMax;
    }


//############################      Vetor Ordenado      ############################
    template <class Chave, class Item>
    class vetorOrd{
        private:
            no_vector<Chave, Item> *vector;
            int n;
            int tamMax;
            void resize();
        public:
            vetorOrd(string nomeDoTexto);
            ~vetorOrd();
            void insere (Chave chave, Item valor);
            Item devolve (Chave chave);
            void remove (Chave chave);
            int rank (Chave chave);
            Chave seleciona(int k);
    };

    template <class Chave, class Item>
    vetorOrd<Chave, Item>::vetorOrd(string nomeDoTexto){
        vector = new no_vector<Chave, Item>[1];
        n = 0;
        tamMax = 1;

        fstream texto;
        string word;
        texto.open(nomeDoTexto);
        while (texto >> word){
            word = getOnlyWord(word);
            insere(word, 1);
        }
        texto.close();
    }

    template <class Chave, class Item>
    vetorOrd<Chave, Item>::~vetorOrd(){
        delete [] vector;
    }

    template <class Chave, class Item>
    void vetorOrd<Chave, Item>::insere(Chave chave, Item valor){
        int indice = rank(chave);

        if (indice < n && vector[indice].getChave() == chave){
            vector[indice].setValor(vector[indice].getValor()+valor);
            return;
        }

        if (n == tamMax)
            resize();


        for (int i = n; i > indice; i--){
            vector[i] = vector[i-1];
        }
        vector[indice].setValor(valor);
        vector[indice].setChave(chave); 
        n++;
    }

    template <class Chave, class Item>
    Item vetorOrd<Chave, Item>::devolve(Chave chave){
        int indice = rank(chave);
        if (indice < n && vector[indice].getChave() == chave)
            return (vector[indice].getValor());
        return 0;
    }

    template <class Chave, class Item>
    void vetorOrd<Chave, Item>::remove(Chave chave){
        int indice = rank(chave);
        if (indice < n && vector[indice].getChave() == chave){
            for (int i = indice; i < n-1; i++){
                vector[i] = vector[i+1];
            }
            n--;
        }
    }

    template <class Chave, class Item>
    int vetorOrd<Chave, Item>::rank(Chave chave){
        int meio, inicio=0, fim=n-1;
        while (inicio <= fim){
            meio = (inicio+fim)/2;
            if (vector[meio].getChave() == chave)
                return meio;
            else if (vector[meio].getChave() > chave)
                fim = meio-1;
            else
                inicio = meio+1;
        }
        return inicio;
    }

    template <class Chave, class Item>
    Chave vetorOrd<Chave, Item>::seleciona(int k){
        if (k < n)
            return (vector[k].getChave());
        return "";
    }

    //Funções privadas

    template <class Chave, class Item>
    void vetorOrd<Chave, Item>::resize(){
        no_vector<Chave, Item> *aux = new no_vector<Chave, Item>[2*tamMax];
        for (int i=0; i < tamMax; i++)
            aux[i] = vector[i];
        delete [] vector;
        vector = aux;
        tamMax = 2*tamMax;
    }


#endif