#include "util.hpp"
#include "vector.hpp"
using namespace std;

#ifndef ABB_H
#define ABB_H

//############################     Declaração dos Nós   ############################
    template <class Chave, class Item>
    class no_abb : public no_vector<Chave, Item>{
        private:
            no_abb<Chave, Item>* esq, *dir;
        public:
            no_abb();
            no_abb<Chave, Item>* getEsq();
            no_abb<Chave, Item>* getDir();
            void setEsq(no_abb<Chave, Item>*novaEsq);
            void setDir(no_abb<Chave, Item>*novaDir);
    };

    template <class Chave, class Item>
    no_abb<Chave, Item>::no_abb(): esq(nullptr), dir(nullptr){};

    template <class Chave, class Item>
    no_abb<Chave, Item>* no_abb<Chave, Item>::getEsq(){
        return esq;
    }

    template <class Chave, class Item>
    no_abb<Chave, Item>* no_abb<Chave, Item>::getDir(){
        return dir;
    }

    template <class Chave, class Item>
    void no_abb<Chave, Item>::setEsq(no_abb<Chave, Item>*novaEsq){
        esq = novaEsq;
    }

    template <class Chave, class Item>
    void no_abb<Chave, Item>::setDir(no_abb<Chave, Item>*novaDir){
        dir = novaDir;
    }

//############################            ABB           ############################
    template <class Chave, class Item>
    class arvoreBin{
        private:
            no_abb<Chave, Item>* raiz;
            int contaElementos(no_abb<Chave, Item>*noRaiz);
            no_abb<Chave, Item>*getNoByRank(no_abb<Chave, Item>*noRaiz, int rank);
            no_abb<Chave, Item>*putNo(no_abb<Chave, Item>*noRaiz, Chave ch, Item val);
            no_abb<Chave, Item>*deleteNo(no_abb<Chave, Item>*noRaiz, Chave ch);
            no_abb<Chave, Item>*getMaiorNo(no_abb<Chave, Item>*noRaiz);
            void deleteAbb(no_abb<Chave, Item>*noRaiz);
        public:
            arvoreBin(string nomeDoTexto);
            ~arvoreBin();
            void insere (Chave chave, Item valor);
            Item devolve (Chave chave);
            void remove (Chave chave);
            int rank (Chave chave);
            Chave seleciona(int k);
    };

    template <class Chave, class Item>
    arvoreBin<Chave, Item>::arvoreBin(string nomeDoTexto){
        raiz = nullptr;

        fstream texto;
        string word;
        texto.open(nomeDoTexto);
        while (texto >> word){
            word = getOnlyWord(word);
            insere(word, 1);
        }
        texto.close();
    }

    template <class Chave, class Item>
    arvoreBin<Chave, Item>::~arvoreBin(){
        deleteAbb(raiz);
    }

    template <class Chave, class Item>
    void arvoreBin<Chave, Item>::insere(Chave chave, Item valor){
        raiz = putNo(raiz, chave, valor);
    }

    template <class Chave, class Item>
    Item arvoreBin<Chave, Item>::devolve(Chave chave){
        no_abb<Chave, Item>* aux = raiz;
        while (aux != nullptr && aux->getChave() != chave){
            if (aux->getChave() > chave)
                aux = aux->getEsq();
            else if (aux->getChave() < chave)
                aux = aux->getDir();
        }
        if (aux != nullptr)
            return (aux->getValor());
        return 0;
    }

    template <class Chave, class Item>
    void arvoreBin<Chave, Item>::remove(Chave chave){
        raiz = deleteNo(raiz, chave);
    }

    template <class Chave, class Item>
    int arvoreBin<Chave, Item>::rank(Chave chave){
        no_abb<Chave, Item>* aux=raiz;
        int k=0;
        while (aux != nullptr && aux->getChave() != chave){
            if (aux->getChave() > chave)
                aux = aux->getEsq();
            else if (aux->getChave() < chave){
                k += contaElementos(aux->getEsq());
                k += 1; //Raiz
                aux = aux->getDir();
            }
        }
        if (aux != nullptr){
            //A chave existe na ABB
            k += contaElementos(aux->getEsq());
        }

        return k;
    }

    template <class Chave, class Item>
    Chave arvoreBin<Chave, Item>::seleciona(int k){
        no_abb<Chave, Item>* aux;
        aux = getNoByRank(raiz, k);
        if (aux != nullptr)
            return aux->getChave();
        return "";
    }

    //Funções privadas

    /*Conta o número de elementos abaixo do "noRaiz", com o próprio nó incluso na conta*/
    template <class Chave, class Item>
    int arvoreBin<Chave, Item>::contaElementos(no_abb<Chave, Item>*noRaiz){
        if (noRaiz!= nullptr){
            int esq, dir;
            esq = contaElementos(noRaiz->getEsq());
            dir = contaElementos(noRaiz->getDir());
            return esq+dir+1;
        }
        return 0;
    }

    /*Busca o nó na árvore através do rank*/
    template <class Chave, class Item>
    no_abb<Chave, Item>* arvoreBin<Chave, Item>::getNoByRank(no_abb<Chave, Item>*noRaiz, int k){
        if (noRaiz == nullptr)
            return nullptr;
        int rankAtual = rank(noRaiz->getChave());
        if (rankAtual > k)
            return getNoByRank(noRaiz->getEsq(), k);
        if (rankAtual < k)
            return getNoByRank(noRaiz->getDir(), k);
        return noRaiz;
    }

    /*Insere o nó na árvore recursivamente*/
    template <class Chave, class Item>
    no_abb<Chave, Item>* arvoreBin<Chave, Item>::putNo(no_abb<Chave, Item>*noRaiz, Chave ch, Item val){
        if (noRaiz == nullptr){
            no_abb<Chave, Item>*novo = new no_abb<Chave, Item>;
            novo->setChave(ch);
            novo->setValor(val);
            return novo;
        }
        if (noRaiz->getChave() == ch)
            noRaiz->setValor(noRaiz->getValor()+val);

        else if (noRaiz->getChave() > ch)
            noRaiz->setEsq(putNo(noRaiz->getEsq(), ch, val));

        else if (noRaiz->getChave() < ch)
            noRaiz->setDir(putNo(noRaiz->getDir(), ch, val));
        
        return noRaiz;
    }

    /*Delete o nó da árvore recursivamente*/
    template <class Chave, class Item>
    no_abb<Chave, Item>* arvoreBin<Chave, Item>::deleteNo(no_abb<Chave, Item>*noRaiz, Chave ch){
        if (noRaiz == nullptr)
            return noRaiz;
        if (noRaiz->getChave() > ch)
            noRaiz->setEsq(deleteNo(noRaiz->getEsq(), ch));
        else if(noRaiz->getChave() < ch)
            noRaiz->setDir(deleteNo(noRaiz->getDir(), ch));
        else{
            if (noRaiz->getEsq() == nullptr){
                no_abb<Chave, Item>* aux=noRaiz->getDir();
                delete noRaiz;
                return aux;
            }
            if (noRaiz->getDir() == nullptr){
                no_abb<Chave, Item>* aux=noRaiz->getEsq();
                delete noRaiz;
                return aux;
            }
            no_abb<Chave, Item>* aux= getMaiorNo(noRaiz->getEsq());
            noRaiz->setChave(aux->getChave());
            noRaiz->setValor(aux->getValor());
            noRaiz->setEsq(deleteNo(noRaiz->getEsq(), aux->getChave()));
        }
        return noRaiz;
    }

    /*Retorna o maior nó da árvore de raiz "noRaiz"*/
    template <class Chave, class Item>
    no_abb<Chave, Item>* arvoreBin<Chave, Item>::getMaiorNo(no_abb<Chave, Item>*noRaiz){
        if (noRaiz->getDir() != nullptr)
            return getMaiorNo(noRaiz->getDir());
        return noRaiz;
    }

    template <class Chave, class Item>
    void arvoreBin<Chave, Item>::deleteAbb(no_abb<Chave, Item>*noRaiz){
        if (noRaiz != nullptr){
            deleteAbb(noRaiz->getEsq());
            deleteAbb(noRaiz->getDir());
            delete noRaiz;
        }
    }

#endif