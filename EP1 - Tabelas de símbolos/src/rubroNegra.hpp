#include "util.hpp"
using namespace std;

#ifndef RUBRONEGRA_H
#define RUBRONEGRA_H

#define RED "vermelho"
#define BLACK "preto"

//############################     Declaração dos Nós   ############################
    template <class Chave, class Item>
    class no_rn : public no_vector<Chave, Item>{
        private:
            no_rn<Chave, Item>*esq, *dir,*pai;
            bool cor; // true = Vermelho | False = Preto
        public:
            no_rn();
            no_rn<Chave, Item>* getEsq();
            no_rn<Chave, Item>* getDir();
            no_rn<Chave, Item>* getPai();
            void setEsq(no_rn<Chave, Item>*novaEsq);
            void setDir(no_rn<Chave, Item>*novaDir);
            void setPai(no_rn<Chave, Item>*novoPai);
            bool noVermelho();
            void pintaNo(string nomeCor);
    };

    template <class Chave, class Item>
    no_rn<Chave, Item>::no_rn(): esq(nullptr), dir(nullptr), pai(nullptr), cor(true){};

    template <class Chave, class Item>
    no_rn<Chave, Item>* no_rn<Chave, Item>::getEsq(){
        return esq;
    }

    template <class Chave, class Item>
    no_rn<Chave, Item>* no_rn<Chave, Item>::getDir(){
        return dir;
    }

    template <class Chave, class Item>
    no_rn<Chave, Item>* no_rn<Chave, Item>::getPai(){
        return pai;
    }

    template <class Chave, class Item>
    void no_rn<Chave, Item>::setEsq(no_rn<Chave, Item>*novaEsq){
        esq = novaEsq;
    }

    template <class Chave, class Item>
    void no_rn<Chave, Item>::setDir(no_rn<Chave, Item>*novaDir){
        dir = novaDir;
    }

    template <class Chave, class Item>
    void no_rn<Chave, Item>::setPai(no_rn<Chave, Item>*novoPai){
        pai = novoPai;
    }

    template <class Chave, class Item>
    bool no_rn<Chave, Item>::noVermelho(){
        return cor;
    }

    template <class Chave, class Item>
    void no_rn<Chave, Item>::pintaNo(string nomeCor){
        if (nomeCor == "vermelho")
            cor = true;
        else if (nomeCor == "preto")
            cor = false;
    }

//############################    Árvore Rubro-Negra    ############################
    template <class Chave, class Item>
    class arvoreRN{
        private:
            no_rn<Chave, Item>*raiz;
            int contaElementos(no_rn<Chave, Item>*noRaiz);
            no_rn<Chave, Item>*getNoByRank(no_rn<Chave, Item>*noRaiz, int k);
            no_rn<Chave, Item>*getMaiorNo(no_rn<Chave, Item>*noRaiz);
            void deleteNo(no_rn<Chave, Item>*noRaiz, Chave ch);
            void rotaciona(no_rn<Chave, Item>*noRaiz);
            void deleteRN(no_rn<Chave, Item>*noRaiz);
            void printArvore(no_rn<Chave, Item>*noRaiz);
        public:
            void print();
            arvoreRN(string nomeDoTexto);
            ~arvoreRN();
            void insere (Chave chave, Item valor);
            Item devolve (Chave chave);
            void remove (Chave chave);
            int rank (Chave chave);
            Chave seleciona(int k);
    };

    template <class Chave, class Item>
    arvoreRN<Chave, Item>::arvoreRN(string nomeDoTexto){
        raiz = nullptr;

        fstream texto;
        string word;
        texto.open(nomeDoTexto);
        while (texto >> word){
            word = getOnlyWord(word);
            insere(word, 1);
        }
        texto.close();
    }

    template <class Chave, class Item>
    arvoreRN<Chave, Item>::~arvoreRN(){
        deleteRN(raiz);
    }

    template <class Chave, class Item>
    void arvoreRN<Chave, Item>::insere(Chave chave, Item valor){
        if (raiz == nullptr){
            raiz = new no_rn<Chave, Item>;
            raiz->setChave(chave);
            raiz->setValor(valor);
            return;
        }
        no_rn<Chave, Item>*pai=raiz;
        bool achou=false;
        while (!achou){
            if (pai->getChave() > chave && pai->getEsq() != nullptr)
                pai = pai->getEsq(); 
            else if (pai->getChave() < chave && pai->getDir() != nullptr)
                pai = pai->getDir();
            else if (pai->getChave() == chave){
                pai->setValor(pai->getValor()+valor);
                return;
            }
            else
                achou=true;
        }
        no_rn<Chave, Item>*novo = new no_rn<Chave, Item>;
        novo->setChave(chave);
        novo->setValor(valor);
        novo->setPai(pai);
        if (pai->getChave() > chave)
            pai->setEsq(novo);
        else
            pai->setDir(novo);

        while (pai != nullptr){
            if (!pai->noVermelho()){
                //O Pai é preto
                return;
            }
            if (pai->getPai() == nullptr){
                //A Raiz é vermelha, ou seja, pode pintar de preto
                pai->pintaNo(BLACK);
                return;
            }
            no_rn<Chave, Item>*tio;
            if (pai->getPai()->getDir() == pai)
                tio = pai->getPai()->getEsq();
            else
                tio = pai->getPai()->getDir();
            if (tio != nullptr && tio->noVermelho()){
                //O tio é vermelho, ou seja, podemos pintar o pai e o tio de preto e o avo de vermelho
                tio->pintaNo(BLACK);
                pai->pintaNo(BLACK);
                pai->getPai()->pintaNo(RED);
                novo = pai->getPai();
                pai = novo->getPai();
            }
            else{
                no_rn<Chave, Item>*avo = pai->getPai();
                if (avo->getDir() == pai && pai->getEsq() == novo){
                    //Se o pai é filho direito e o novo é filho esquerdo, ou seja, rotação para a direita
                    pai->setEsq(novo->getDir());
                    if (novo->getDir() != nullptr)
                        novo->getDir()->setPai(pai);
                    novo->setDir(pai);
                    novo->setPai(avo);
                    pai->setPai(novo);
                    avo->setDir(novo);
                    novo = pai;
                    pai = novo->getPai();
                    avo = pai->getPai();
                }
                else if(avo->getEsq() == pai && pai->getDir() == novo){
                    //Se o pai é filho esquerdo e o novo é filho direito, ou seja, rotação para a esquerda
                    pai->setDir(novo->getEsq());
                    if (novo->getEsq() != nullptr)
                        novo->getEsq()->setPai(pai);
                    novo->setEsq(pai);
                    novo->setPai(avo);
                    pai->setPai(novo);
                    avo->setEsq(novo);
                    novo = pai;
                    pai = novo->getPai();
                    avo = pai->getPai();
                }
                if (avo->getEsq() == pai && pai->getEsq() == novo){
                    //Se o pai é filho esquerdo e o novo é filho esquerdo, ou seja, rotação para a direita
                    avo->setEsq(pai->getDir());
                    if (pai->getDir() != nullptr)
                        pai->getDir()->setPai(avo); 
                    pai->setDir(avo);
                    pai->setPai(avo->getPai());
                    if (avo == raiz)
                        raiz = pai;
                    else if (avo->getPai()->getDir() == avo)
                        avo->getPai()->setDir(pai);
                    else
                        avo->getPai()->setEsq(pai);
                    avo->setPai(pai);
                    avo->pintaNo(RED);
                    pai->pintaNo(BLACK);
                    return;
                }
                else if (avo->getDir() == pai && pai->getDir() == novo){
                    //Se o pai é filho direito e o novo é filho direito, ou seja, rotação para a esquerda
                    avo->setDir(pai->getEsq());
                    if (pai->getEsq() != nullptr)
                        pai->getEsq()->setPai(avo);
                    pai->setEsq(avo);
                    pai->setPai(avo->getPai());
                    if (avo == raiz)
                        raiz = pai;
                    else if (avo->getPai()->getDir() == avo)
                        avo->getPai()->setDir(pai);
                    else
                        avo->getPai()->setEsq(pai);
                    avo->setPai(pai);
                    avo->pintaNo(RED);
                    pai->pintaNo(BLACK);
                    return;
                }
            }
        }
    }

    template <class Chave, class Item>
    Item arvoreRN<Chave, Item>::devolve(Chave chave){
        no_rn<Chave, Item>* aux = raiz;
        while (aux != nullptr && aux->getChave() != chave){
            if (aux->getChave() > chave)
                aux = aux->getEsq();
            else if (aux->getChave() < chave)
                aux = aux->getDir();
        }
        if (aux != nullptr)
            return (aux->getValor());
        return 0;
    }

    template <class Chave, class Item>
    void arvoreRN<Chave, Item>::remove(Chave chave){
        deleteNo(raiz, chave);
    }

    template <class Chave, class Item>
    int arvoreRN<Chave, Item>::rank(Chave chave){
        no_rn<Chave, Item>* aux=raiz;
        int k=0;
        while (aux != nullptr && aux->getChave() != chave){
            if (aux->getChave() > chave)
                aux = aux->getEsq();
            else if (aux->getChave() < chave){
                k += contaElementos(aux->getEsq());
                k += 1; //Raiz
                aux = aux->getDir();
            }
        }
        if (aux != nullptr){
            //A chave existe na árvore
            k += contaElementos(aux->getEsq());
        }

        return k;
    }

    template <class Chave, class Item>
    Chave arvoreRN<Chave, Item>::seleciona(int k){
        no_rn<Chave, Item>* aux;
        aux = getNoByRank(raiz, k);
        if (aux != nullptr)
            return aux->getChave();
        return "";
    }

    //Funções privadas

    /*Conta o número de elementos abaixo do "noRaiz", com o próprio nó incluso na conta*/
    template <class Chave, class Item>
    int arvoreRN<Chave, Item>::contaElementos(no_rn<Chave, Item>*noRaiz){
        if (noRaiz!= nullptr){
            int esq, dir;
            esq = contaElementos(noRaiz->getEsq());
            dir = contaElementos(noRaiz->getDir());
            return esq+dir+1;
        }
        return 0;
    }

    /*Busca o nó na árvore através do rank*/
    template <class Chave, class Item>
    no_rn<Chave, Item>* arvoreRN<Chave, Item>::getNoByRank(no_rn<Chave, Item>*noRaiz, int k){
        if (noRaiz == nullptr)
            return nullptr;
        int rankAtual = rank(noRaiz->getChave());
        if (rankAtual > k)
            return getNoByRank(noRaiz->getEsq(), k);
        if (rankAtual < k)
            return getNoByRank(noRaiz->getDir(), k);
        return noRaiz;
    }

    /*Retorna o maior nó da árvore de raiz "noRaiz"*/
    template <class Chave, class Item>
    no_rn<Chave, Item>* arvoreRN<Chave, Item>::getMaiorNo(no_rn<Chave, Item>*noRaiz){
        if (noRaiz->getDir() != nullptr)
            return getMaiorNo(noRaiz->getDir());
        return noRaiz;
    }

    /*Deleta o nó da árvore*/
    template <class Chave, class Item>
    void arvoreRN<Chave, Item>::deleteNo(no_rn<Chave, Item>*noRaiz, Chave chave){
       no_rn<Chave, Item>* aux=noRaiz;
        if (noRaiz == nullptr)
            return;
        while (aux != nullptr && aux->getChave() != chave){
            if (aux->getChave() > chave)
                aux = aux->getEsq();
            else
                aux = aux->getDir();
        }
        if (aux->getDir() == nullptr && aux->getEsq() == nullptr){
            //é folha
            if (aux->noVermelho() || aux == raiz){
                if (aux == raiz){
                    //Caso particular de árvore com só 1 nó
                    raiz = nullptr;
                }
                else{
                    if (aux->getPai()->getEsq() == aux)
                        aux->getPai()->setEsq(nullptr);
                    else
                        aux->getPai()->setDir(nullptr);
                }
                delete aux;
                return;
            }
            no_rn<Chave, Item>*pai, *irmao;
            no_rn<Chave, Item>*duploPreto = aux;
            bool irmaoDireito;

            while (duploPreto != nullptr && duploPreto != raiz){
                pai = duploPreto->getPai();
                if (pai->getEsq() == duploPreto){
                    irmao = pai->getDir();
                    irmaoDireito = true;
                }
                else{
                    irmao = pai->getEsq();
                    irmaoDireito = false;
                }

                if (irmao->noVermelho()){
                    //Irmão vermelho
                    irmao->pintaNo(BLACK);
                    pai->pintaNo(RED);
                    rotaciona(irmao);
                }
                else{
                    if ((irmao->getEsq() == nullptr || !irmao->getEsq()->noVermelho()) &&
                    (irmao->getDir() == nullptr || !irmao->getDir()->noVermelho())){
                        //Irmão preto com filhos pretos
                        irmao->pintaNo(RED);
                        if (duploPreto == aux){
                            if (irmaoDireito)
                                pai->setEsq(nullptr);
                            else
                                pai->setDir(nullptr);
                            delete aux;
                        }
                        if (pai->noVermelho()){
                            pai->pintaNo(BLACK);
                            duploPreto = nullptr;
                        }
                        else
                            duploPreto = pai;
                    }
                    else if ((irmaoDireito && irmao->getDir() != nullptr && irmao->getDir()->noVermelho()) ||
                     (!irmaoDireito && irmao->getEsq() != nullptr && irmao->getEsq()->noVermelho())){
                        //Irmão preto com sobrinho mais longe vermelho
                        if (pai->noVermelho())
                            irmao->pintaNo(RED);
                        else
                            irmao->pintaNo(BLACK);
                        pai->pintaNo(BLACK);
                        rotaciona(irmao);
                        if (irmaoDireito)
                            irmao->getDir()->pintaNo(BLACK);
                        else
                            irmao->getEsq()->pintaNo(BLACK);
                        if (duploPreto == aux){
                            if (irmaoDireito)
                                pai->setEsq(nullptr);
                            else
                                pai->setDir(nullptr);
                            delete aux;
                        }
                        duploPreto = nullptr;
                    }
                    else{
                        //Irmão preto com sobrinho mais próximo vermelho e sobrinho mais longe preto
                        irmao->pintaNo(RED);
                        if (irmaoDireito){
                            irmao->getEsq()->pintaNo(BLACK);
                            rotaciona(irmao->getEsq());
                        }
                        else{
                            irmao->getDir()->pintaNo(BLACK);
                            rotaciona(irmao->getDir());
                        }
                    }
                }
            }
        }
        else if (aux->getDir() == nullptr){
            aux->setChave(aux->getEsq()->getChave());
            aux->setValor(aux->getEsq()->getValor());
            delete (aux->getEsq());
            aux->setEsq(nullptr);
        }
        else if (aux->getEsq() == nullptr){
            aux->setChave(aux->getDir()->getChave());
            aux->setValor(aux->getDir()->getValor());
            delete (aux->getDir());
            aux->setDir(nullptr);
        }
        else{
            no_rn<Chave, Item>*lixo = getMaiorNo(aux->getEsq());
            aux->setChave(lixo->getChave());
            aux->setValor(lixo->getValor());
            deleteNo(aux->getEsq(), lixo->getChave());
        }
    }

    template <class Chave, class Item>
    void arvoreRN<Chave, Item>::rotaciona(no_rn<Chave, Item>*noRaiz){
        no_rn<Chave, Item>*pai = noRaiz->getPai();
        noRaiz->setPai(pai->getPai());
        if (pai->getEsq() == noRaiz){
            /*Rotação para a direita*/
            pai->setEsq(noRaiz->getDir());
            if (noRaiz->getDir() != nullptr)
                noRaiz->getDir()->setPai(pai);
            noRaiz->setDir(pai);
            pai->setPai(noRaiz);
        }
        else{
            /*Rotação para a esquerda*/
            pai->setDir(noRaiz->getEsq());
            if (noRaiz->getEsq() != nullptr)
                noRaiz->getEsq()->setPai(pai);
            noRaiz->setEsq(pai);
            pai->setPai(noRaiz);
        }
        if (noRaiz->getPai() == nullptr)
            raiz = noRaiz;
        else if (noRaiz->getPai()->getEsq() == pai){
            //Se o pai, antes de rotacionar, era filho esquerdo
            noRaiz->getPai()->setEsq(noRaiz);
        }
        else{
            //Se o pai, antes de rotacionar, era filho dirieto
            noRaiz->getPai()->setDir(noRaiz);
        }
    }

    template <class Chave, class Item>
    void arvoreRN<Chave, Item>::deleteRN(no_rn<Chave, Item>*noRaiz){
        if (noRaiz != nullptr){
            deleteRN(noRaiz->getEsq());
            deleteRN(noRaiz->getDir());
            delete noRaiz;
        }
    }

    template <class Chave, class Item>
    void arvoreRN<Chave, Item>::print(){
        cout<<"\n##PRINT ARVORE##";
        printArvore(raiz);
        cout<<"\n";
    }
    
    template <class Chave, class Item>
    void arvoreRN<Chave, Item>::printArvore(no_rn<Chave, Item>*noRaiz){
        if (noRaiz != nullptr){
            printArvore(noRaiz->getEsq());
            cout<<"\nChave: "<<noRaiz->getChave()<<" |Valor: "<<noRaiz->getValor();
            printArvore(noRaiz->getDir());
        }
    }
    

#endif