#include "util.hpp"
#include "vector.hpp"
using namespace std;

#ifndef LINKEDLIST_H
#define LINKEDLIST_H

//############################     Declaração dos Nós   ############################
    template <class Chave, class Item>
    class no_lista : public no_vector<Chave, Item>{
        private:
            no_lista<Chave, Item>* prox;
        public:
            no_lista();
            no_lista<Chave, Item>* getProx();
            void setProx(no_lista<Chave, Item>*novoProx);
    };

    template <class Chave, class Item>
    no_lista<Chave, Item>::no_lista(): prox(nullptr){};

    template <class Chave, class Item>
    no_lista<Chave, Item>* no_lista<Chave, Item>::getProx(){
        return prox;
    }

    template <class Chave, class Item>
    void no_lista<Chave, Item>::setProx(no_lista<Chave, Item>*novoProx){
        prox = novoProx;
    }

//############################ Lista ligada Desordenada ############################
    template <class Chave, class Item>
    class listaDes{
        private:
            no_lista<Chave, Item>*ini;
        public:
            listaDes(string nomeDoTexto);
            ~listaDes();
            void insere (Chave chave, Item valor);
            Item devolve (Chave chave);
            void remove (Chave chave);
            int rank (Chave chave);
            Chave seleciona(int k);
    };

    template <class Chave, class Item>
    listaDes<Chave, Item>::listaDes(string nomeDoTexto){
        ini = nullptr;

        fstream texto;
        string word;
        texto.open(nomeDoTexto);
        while (texto >> word){
            word = getOnlyWord(word);
            insere(word, 1);
        }
        texto.close();
    }

    template <class Chave, class Item>
    listaDes<Chave, Item>::~listaDes(){
        no_lista<Chave, Item>* aux;
        while (ini != nullptr){
            aux = ini;
            ini = ini->getProx();
            delete aux;
        }
    }

    template <class Chave, class Item>
    void listaDes<Chave, Item>::insere(Chave chave, Item valor){
        no_lista<Chave, Item>* aux=ini, *novo;
        while (aux != nullptr){
            if (aux->getChave() == chave){
                aux->setValor(aux->getValor()+valor);
                return;
            }
            aux = aux->getProx();
        }

        novo = new no_lista<Chave, Item>;
        novo->setValor(valor);
        novo->setChave(chave);
        novo->setProx(ini);
        ini = novo;
    }

    template <class Chave, class Item>
    Item listaDes<Chave, Item>::devolve(Chave chave){
        no_lista<Chave, Item>* aux=ini;
        while (aux != nullptr){
            if (aux->getChave() == chave)
                return (aux->getValor());
            aux = aux->getProx();
        }
        return 0;
    }

    template <class Chave, class Item>
    void listaDes<Chave, Item>::remove(Chave chave){
        no_lista<Chave, Item>* aux=ini, *lixo;
        if (ini == nullptr)
            return;
        if (ini->getChave() == chave){
            lixo = ini;
            ini = ini->getProx();
            delete lixo;
            return;
        }
        while (aux->getProx() != nullptr && aux->getProx()->getChave() != chave){
            aux = aux->getProx();
        }

        if (aux->getProx() != nullptr){
            lixo = aux->getProx();
            aux->setProx(lixo->getProx());
            delete lixo;
        }
    }

    template <class Chave, class Item>
    int listaDes<Chave, Item>::rank(Chave chave){
        no_lista<Chave, Item>* aux=ini;
        int k=0;
        while (aux != nullptr){
            if (aux->getChave() < chave)
                k++;
            aux = aux->getProx();
        }
        return k;
    }

    template <class Chave, class Item>
    Chave listaDes<Chave, Item>::seleciona(int k){
        no_lista<Chave, Item>* aux=ini;
        while (aux != nullptr){
            if (rank(aux->getChave()) == k)
                return (aux->getChave());
            aux = aux->getProx();
        }
        return "";
    }


//############################   Lista ligada Ordenada  ############################
    template <class Chave, class Item>
    class listaOrd{
        private:
            no_lista<Chave, Item>*ini;
        public:
            listaOrd(string nomeDoTexto);
            ~listaOrd();
            void insere (Chave chave, Item valor);
            Item devolve (Chave chave);
            void remove (Chave chave);
            int rank (Chave chave);
            Chave seleciona(int k);
    };

    template <class Chave, class Item>
    listaOrd<Chave, Item>::listaOrd(string nomeDoTexto){
        ini = nullptr;

        fstream texto;
        string word;
        texto.open(nomeDoTexto);
        while (texto >> word){
            word = getOnlyWord(word);
            insere(word, 1);
        }
        texto.close();
    }

    template <class Chave, class Item>
    listaOrd<Chave, Item>::~listaOrd(){
        no_lista<Chave, Item>* aux;
        while (ini != nullptr){
            aux = ini;
            ini = ini->getProx();
            delete aux;
        }
    }

    template <class Chave, class Item>
    void listaOrd<Chave, Item>::insere(Chave chave, Item valor){
        no_lista<Chave, Item> *aux=ini, *novo;
        if (ini == nullptr || ini->getChave() >= chave){
            //Insere no inicio
            if (ini != nullptr && ini->getChave() == chave){
                ini->setValor(aux->getValor()+valor);
                return;
            }
            novo = new (no_lista<Chave, Item>);
            novo->setChave(chave);
            novo->setValor(valor);
            novo->setProx(ini);
            ini = novo;
            return;
        }

        while (aux->getProx() != nullptr && aux->getProx()->getChave() < chave){
            aux = aux->getProx();
        }

        if (aux->getProx() != nullptr && aux->getProx()->getChave() == chave){
            aux->getProx()->setValor(aux->getProx()->getValor()+valor);
            return;
        }
        novo = new no_lista<Chave, Item>;
        novo->setChave(chave);
        novo->setValor(valor);
        novo->setProx(aux->getProx());
        aux->setProx(novo);
    }

    template <class Chave, class Item>
    Item listaOrd<Chave, Item>::devolve(Chave chave){
        no_lista<Chave, Item> *aux=ini;
        while (aux != nullptr && aux->getChave() != chave){
            aux = aux->getProx();
        }
        if (aux != nullptr)
            return (aux->getValor());
        return 0;
    }

    template <class Chave, class Item>
    void listaOrd<Chave, Item>::remove(Chave chave){
        no_lista<Chave, Item>* aux=ini, *lixo;
        if (ini == nullptr)
            return;
        if (ini->getChave() == chave){
            lixo = ini;
            ini = ini->getProx();
            delete lixo;
            return;
        }
        while (aux->getProx() != nullptr && aux->getProx()->getChave() != chave){
            aux = aux->getProx();
        }

        if (aux->getProx() != nullptr){
            lixo = aux->getProx();
            aux->setProx(lixo->getProx());
            delete lixo;
        }
    }

    template <class Chave, class Item>
    int listaOrd<Chave, Item>::rank(Chave chave){
        no_lista<Chave, Item>*aux=ini;
        int k=0;
        while (aux != nullptr && aux->getChave() < chave){
            aux = aux->getProx();
            k++;
        }
        return k;
    }

    template <class Chave, class Item>
    Chave listaOrd<Chave, Item>::seleciona(int k){
        no_lista<Chave, Item>* aux=ini;
        while (aux != nullptr && k > 0){
            aux = aux->getProx();
            k--;
        }
        if (aux != nullptr)
            return (aux->getChave());
        return "";
    }
    
#endif