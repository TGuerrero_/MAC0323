#include <iostream>
#include <fstream>
using namespace std;

#ifndef UTIL_H
#define UTIL_H

#define NOTDONE cout<<"\nOPS! Essa função ainda não foi feita!";

/*-------------------------------------------------------------*/
/*Declaração de funções auxiliares*/

/*
* getOnlyWord()
* Recebe uma string "input" e remove todos os simbolos
* que não são letras do início e do fim da palavra.
* Retona a nova string.
*/
string getOnlyWord(string input);

/*
* isSymbol()
* Retorna true se o "input" é um simbolo e não uma letra.
*/
bool isSymbol(char input);

#endif