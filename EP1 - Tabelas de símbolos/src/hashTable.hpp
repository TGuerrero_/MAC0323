#include "util.hpp"
#include "linkedList.hpp"
using namespace std;

#ifndef HASHTABLE_H
#define HASHTABLE_H

//############################    Tabela de Hashing     ############################
    template <class Chave, class Item>
    class hashTable{
        private:
            no_lista<Chave, Item>**vector;
            int tableSize;
            int hash(Chave chave);
        public:
            void printa();
            hashTable(string nomeDoTexto);
            ~hashTable();
            void insere (Chave chave, Item valor);
            Item devolve (Chave chave);
            void remove (Chave chave);
            int rank (Chave chave);
            Chave seleciona(int k);
    };

    template <class Chave, class Item>
    hashTable<Chave, Item>::hashTable(string nomeDoTexto){
        fstream texto;
        string word;
        tableSize = 7000;
        vector = new no_lista<Chave, Item>*[tableSize];
        for (int i=0; i < tableSize; i++)
            vector[i] = nullptr;

        texto.open(nomeDoTexto);
        while (texto >> word){
            word = getOnlyWord(word);
            insere(word, 1);
        }
        texto.close();
    }

    template <class Chave, class Item>
    hashTable<Chave, Item>::~hashTable(){
        no_lista<Chave, Item>*aux;
        for (int i=0; i < tableSize; i++){
            aux = vector[i];
            while (aux != nullptr){
                vector[i] = aux;
                aux = aux->getProx();
                delete vector[i];
            }
        }
        delete [] vector;
    }

    template <class Chave, class Item>
    void hashTable<Chave, Item>::insere(Chave chave, Item valor){
        int indice = hash(chave);
        no_lista<Chave, Item>*aux = vector[indice], *novo;
        while (aux != nullptr){
            if (aux->getChave() == chave){
                aux->setValor(aux->getValor()+valor);
                return;
            }
            aux = aux->getProx();
        }
        novo = new no_lista<Chave, Item>;
        novo->setChave(chave);
        novo->setValor(valor);
        novo->setProx(vector[indice]);
        vector[indice] = novo;
    }

    template <class Chave, class Item>
    Item hashTable<Chave, Item>::devolve(Chave chave){
        int indice = hash(chave);
        no_lista<Chave, Item>*aux = vector[indice];
        while (aux != nullptr && aux->getChave() != chave)
            aux = aux->getProx();
        if (aux != nullptr)
            return aux->getValor();
        return 0;
    }

    template <class Chave, class Item>
    void hashTable<Chave, Item>::remove(Chave chave){
        int indice = hash(chave);
        no_lista<Chave, Item>*aux = vector[indice], *lixo;
        if (aux->getChave() == chave){
            vector[indice] = aux->getProx();
            delete aux;
            return;
        }
        while (aux->getProx() != nullptr && aux->getProx()->getChave() != chave)
            aux = aux->getProx();
        if (aux->getProx() != nullptr){
            lixo = aux->getProx();
            aux->setProx(lixo->getProx());
            delete lixo;
        }
    }

    template <class Chave, class Item>
    int hashTable<Chave, Item>::rank(Chave chave){
        no_lista<Chave,Item>*aux;
        int k=0;
        for (int i=0; i < tableSize; i++){
            aux = vector[i];
            while (aux != nullptr){
                if (aux->getChave() < chave)
                    k++;
                aux = aux->getProx();
            }
        }
        return k;
    }

    template <class Chave, class Item>
    Chave hashTable<Chave, Item>::seleciona(int k){
        no_lista<Chave,Item>*aux;
        for (int i=0; i < tableSize; i++){
            aux = vector[i];
            while (aux != nullptr){
                if (rank(aux->getChave()) == k)
                    return aux->getChave();
                aux = aux->getProx();
            }
        }
        return "";
    }

    //Funções privadas

    template <class Chave, class Item>
    int hashTable<Chave, Item>::hash(Chave chave){
        int h=0;
        for (int i=0; i < (int)chave.length(); i++){
            if (chave[i] > 0){
                h = (h*29 + chave[i]) % tableSize;
            }
        }
        return h;
    }

    template <class Chave, class Item>
    void hashTable<Chave, Item>::printa(){
        no_lista<Chave, Item>*aux;
        cout<<"Tamanho da tabela: "<<tableSize<<endl;
        for (int i=0; i < tableSize; i++){
            aux = vector[i];
            if (i%10 == 0)
                getchar();
            cout<<"Início da lista: "<<endl;
            while (aux != nullptr){
                cout<<"\nChave: "<<aux->getChave()<<" |Valor: "<<aux->getValor()<<endl;
                aux = aux->getProx();
            }
        }
    }

#endif