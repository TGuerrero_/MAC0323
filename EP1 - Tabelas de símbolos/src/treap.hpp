#include "util.hpp"
#include "vector.hpp"
using namespace std;

#ifndef TREAP_H
#define TREAP_H

//############################     Declaração dos Nós   ############################
    template <class Chave, class Item>
    class no_treap : public no_vector<Chave, Item>{
        private:
            no_treap<Chave, Item>*esq, *dir, *pai;
            int priority;
        public:
            no_treap(int range);
            no_treap<Chave, Item>* getEsq();
            no_treap<Chave, Item>* getDir();
            no_treap<Chave, Item>* getPai();
            int getPriority();
            void setEsq(no_treap<Chave, Item>*novaEsq);
            void setDir(no_treap<Chave, Item>*novaDir);
            void setPai(no_treap<Chave, Item>*novoPai);
    };

    template <class Chave, class Item>
    no_treap<Chave, Item>::no_treap(int range): esq(nullptr), dir(nullptr){
        priority = rand()%range;
    };

    template <class Chave, class Item>
    no_treap<Chave, Item>* no_treap<Chave, Item>::getEsq(){
        return esq;
    }

    template <class Chave, class Item>
    no_treap<Chave, Item>* no_treap<Chave, Item>::getDir(){
        return dir;
    }

    template <class Chave, class Item>
    no_treap<Chave, Item>* no_treap<Chave, Item>::getPai(){
        return pai;
    }

    template <class Chave, class Item>
    int no_treap<Chave, Item>::getPriority(){
        return priority;
    }

    template <class Chave, class Item>
    void no_treap<Chave, Item>::setEsq(no_treap<Chave, Item>*novaEsq){
        esq = novaEsq;
    }

    template <class Chave, class Item>
    void no_treap<Chave, Item>::setDir(no_treap<Chave, Item>*novaDir){
        dir = novaDir;
    }

    template <class Chave, class Item>
    void no_treap<Chave, Item>::setPai(no_treap<Chave, Item>*novoPai){
        pai = novoPai;
    }

//############################           Treap          ############################
    template <class Chave, class Item>
    class treap{
        private:
            no_treap<Chave, Item>* raiz;
            int tam, range; //Range: Tamanho máximo de prioridade
            void resizeRange();
            int contaElementos(no_treap<Chave, Item>*noRaiz);
            no_treap<Chave, Item>*getNoByRank(no_treap<Chave, Item>*noRaiz, int rank);
            void rotaciona(no_treap<Chave, Item>*noRaiz);
            no_treap<Chave, Item>*deleteNo(no_treap<Chave, Item>*noRaiz, Chave ch);
            no_treap<Chave, Item>*getMaiorNo(no_treap<Chave, Item>*noRaiz);
            void deleteTreap(no_treap<Chave, Item>*noRaiz);
        public:
            treap(string nomeDoTexto);
            ~treap();
            void insere (Chave chave, Item valor);
            Item devolve (Chave chave);
            void remove (Chave chave);
            int rank (Chave chave);
            Chave seleciona(int k);
    };

    template <class Chave, class Item>
    treap<Chave, Item>::treap(string nomeDoTexto){
        raiz = nullptr;
        range = 50;
        tam = 0;

        fstream texto;
        string word;
        texto.open(nomeDoTexto);
        while (texto >> word){
            word = getOnlyWord(word);
            insere(word, 1);
        }
        texto.close();
    }

    template <class Chave, class Item>
    treap<Chave, Item>::~treap(){
        deleteTreap(raiz);
    }

    template <class Chave, class Item>
    void treap<Chave, Item>::insere(Chave chave, Item valor){
        no_treap<Chave, Item>*pai=raiz;
        bool achou=false;
        if (tam >= (2*range/3))
            resizeRange();
        if (raiz == nullptr){
            raiz = new no_treap<Chave, Item>(range);
            raiz->setChave(chave);
            raiz->setValor(valor);
            tam++;
            return;
        }
        while (!achou){
            if (pai->getChave() == chave){
                pai->setValor(pai->getValor()+valor);
                return;
            }
            if (pai->getChave() > chave && pai->getEsq() != nullptr)
                pai = pai->getEsq();
            else if (pai->getChave() < chave && pai->getDir() != nullptr)
                pai = pai->getDir();
            else
                achou = true;
        }
        no_treap<Chave, Item>* novo = new no_treap<Chave, Item>(range);
        novo->setChave(chave);
        novo->setValor(valor);
        novo->setPai(pai);
        tam++;
        if (pai->getChave() > chave)
            pai->setEsq(novo);
        else
            pai->setDir(novo);
        while (pai != nullptr && pai->getPriority() < novo->getPriority()){
            rotaciona(novo);
            pai = novo->getPai();
        }
    }

    template <class Chave, class Item>
    Item treap<Chave, Item>::devolve(Chave chave){
        no_treap<Chave, Item>* aux = raiz;
        while (aux != nullptr && aux->getChave() != chave){
            if (aux->getChave() > chave)
                aux = aux->getEsq();
            else if (aux->getChave() < chave)
                aux = aux->getDir();
        }
        if (aux != nullptr)
            return (aux->getValor());
        return 0;
    }

    template <class Chave, class Item>
    void treap<Chave, Item>::remove(Chave chave){
        raiz = deleteNo(raiz, chave);
    }

    template <class Chave, class Item>
    int treap<Chave, Item>::rank(Chave chave){
        no_treap<Chave, Item>* aux=raiz;
        int k=0;
        while (aux != nullptr && aux->getChave() != chave){
            if (aux->getChave() > chave)
                aux = aux->getEsq();
            else if (aux->getChave() < chave){
                k += contaElementos(aux->getEsq());
                k += 1; //Raiz
                aux = aux->getDir();
            }
        }
        if (aux != nullptr){
            //A chave existe na Treap
            k += contaElementos(aux->getEsq());
        }

        return k;
    }

    template <class Chave, class Item>
    Chave treap<Chave, Item>::seleciona(int k){
        no_treap<Chave, Item>* aux;
        aux = getNoByRank(raiz, k);
        if (aux != nullptr)
            return aux->getChave();
        return "";
    }

    //Funções privadas

    /*Seta o novo range máximo de prioridade dos elementos da treap*/
    template <class Chave, class Item>
    void treap<Chave, Item>::resizeRange(){
        range = (3/2)*tam;
    }

    /*Conta o número de elementos abaixo do "noRaiz", com o próprio nó incluso na conta*/
    template <class Chave, class Item>
    int treap<Chave, Item>::contaElementos(no_treap<Chave, Item>*noRaiz){
        if (noRaiz!= nullptr){
            int esq, dir;
            esq = contaElementos(noRaiz->getEsq());
            dir = contaElementos(noRaiz->getDir());
            return esq+dir+1;
        }
        return 0;
    }

    /*Busca o nó na árvore através do rank*/
    template <class Chave, class Item>
    no_treap<Chave, Item>* treap<Chave, Item>::getNoByRank(no_treap<Chave, Item>*noRaiz, int k){
        if (noRaiz == nullptr)
            return nullptr;
        int rankAtual = rank(noRaiz->getChave());
        if (rankAtual > k)
            return getNoByRank(noRaiz->getEsq(), k);
        if (rankAtual < k)
            return getNoByRank(noRaiz->getDir(), k);
        return noRaiz;
    }

    /*Rotaciona os elementos da treap para satisfazer a condição de prioridade*/
    template <class Chave, class Item>
    void treap<Chave, Item>::rotaciona(no_treap<Chave, Item>*noRaiz){
        no_treap<Chave, Item>*pai = noRaiz->getPai();
        noRaiz->setPai(pai->getPai());
        if (pai->getEsq() == noRaiz){
            /*Rotação para a direita*/
            pai->setEsq(noRaiz->getDir());
            if (noRaiz->getDir() != nullptr)
                noRaiz->getDir()->setPai(pai);
            noRaiz->setDir(pai);
            pai->setPai(noRaiz);
        }
        else{
            /*Rotação para a esquerda*/
            pai->setDir(noRaiz->getEsq());
            if (noRaiz->getEsq() != nullptr)
                noRaiz->getEsq()->setPai(pai);
            noRaiz->setEsq(pai);
            pai->setPai(noRaiz);
        }
        if (noRaiz->getPai() == nullptr)
            raiz = noRaiz;
        else if (noRaiz->getPai()->getEsq() == pai){
            //Se o pai, antes de rotacionar, era filho esquerdo
            noRaiz->getPai()->setEsq(noRaiz);
        }
        else{
            //Se o pai, antes de rotacionar, era filho direito
            noRaiz->getPai()->setDir(noRaiz);
        }
    }

    /*Delete o nó da árvore recursivamente*/
    template <class Chave, class Item>
    no_treap<Chave, Item>* treap<Chave, Item>::deleteNo(no_treap<Chave, Item>*noRaiz, Chave ch){
        if (noRaiz == nullptr)
            return noRaiz;
        if (noRaiz->getChave() > ch)
            noRaiz->setEsq(deleteNo(noRaiz->getEsq(), ch));
        else if(noRaiz->getChave() < ch)
            noRaiz->setDir(deleteNo(noRaiz->getDir(), ch));
        else{
            if (noRaiz->getEsq() == nullptr){
                no_treap<Chave, Item>* aux=noRaiz->getDir();
                tam--;
                delete noRaiz;
                return aux;
            }
            if (noRaiz->getDir() == nullptr){
                no_treap<Chave, Item>* aux=noRaiz->getEsq();
                tam--;
                delete noRaiz;
                return aux;
            }
            no_treap<Chave, Item>* aux= getMaiorNo(noRaiz->getEsq());
            noRaiz->setChave(aux->getChave());
            noRaiz->setValor(aux->getValor());
            noRaiz->setEsq(deleteNo(noRaiz->getEsq(), aux->getChave()));
        }
        return noRaiz;
    }

    template <class Chave, class Item>
    no_treap<Chave, Item>* treap<Chave, Item>::getMaiorNo(no_treap<Chave, Item>*noRaiz){
        /*Retorna o maior nó da árvore de raiz "noRaiz"*/
        if (noRaiz->getDir() != nullptr)
            return getMaiorNo(noRaiz->getDir());
        return noRaiz;
    }

    template <class Chave, class Item>
    void treap<Chave, Item>::deleteTreap(no_treap<Chave, Item>*noRaiz){
        if (noRaiz != nullptr){
            deleteTreap(noRaiz->getEsq());
            deleteTreap(noRaiz->getDir());
            delete noRaiz;
        }
    }

#endif