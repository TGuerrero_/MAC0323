#include "util.hpp"
#include "vector.hpp"
using namespace std;

#ifndef A23_H
#define A23_H

//############################     Declaração dos Nós   ############################
    template <class Chave, class Item>
    class no_23{
        private:
            no_vector<Chave, Item>*no1, *no2;
            no_23<Chave, Item>*apt1, *apt2, *apt3; 
            bool doisNo; // true = Possui só apt1 e apt2 | false = Possui os 3 apts
        public:
            no_23();
            ~no_23();
            no_vector<Chave, Item>* getNo(int no);
            no_23<Chave, Item>* getApt(int apt);
            void setApt(no_23<Chave, Item>*novoApt, int apt);
            void setNo23(int tipo);
            bool tresNo();
            bool noFolha();
    };

    template <class Chave, class Item>
    no_23<Chave, Item>::no_23(): apt1(nullptr), apt2(nullptr), apt3(nullptr), doisNo(true){
        no1 = new no_vector<Chave, Item>;
        no2 = new no_vector<Chave, Item>;
    };

    template <class Chave, class Item>
    no_23<Chave, Item>::~no_23(){
        delete no1;
        delete no2;
    }

    template <class Chave, class Item>
    no_vector<Chave, Item>* no_23<Chave, Item>::getNo(int no){
        if (no == 1)
            return no1;
        if (no == 2)
            return no2;
        return nullptr;
    }

    template <class Chave, class Item>
    no_23<Chave, Item>* no_23<Chave, Item>::getApt(int apt){
        if (apt == 1)
            return apt1;
        if (apt == 2)
            return apt2;
        return apt3;
    }

    template <class Chave, class Item>
    void no_23<Chave, Item>::setApt(no_23<Chave, Item>*novoApt, int apt){
        /*Recebe o "novoApt" e seta no apontador de indice "apt"*/
        if (apt == 1)
            apt1 = novoApt;
        else if (apt == 2)
            apt2 = novoApt;
        else
            apt3 = novoApt;
    }

    template <class Chave, class Item>
    void no_23<Chave, Item>::setNo23(int tipo){
        /*
        2 - Seta o nó como um 2-Nó
        3 - Seta o nó como um 3-Nó
        */
        if (tipo == 2)
            doisNo = true;
        else if (tipo == 3)
            doisNo = false;
    }

    template <class Chave, class Item>
    bool no_23<Chave, Item>::tresNo(){
        return (!doisNo);
    }

    template <class Chave, class Item>
    bool no_23<Chave, Item>::noFolha(){
        return (apt1 == nullptr && apt2 == nullptr && apt3 == nullptr);
    }

//############################        Árvore 2-3        ############################
    template <class Chave, class Item>
    class arvore23{
        private:
            no_23<Chave, Item>* raiz;
            no_23<Chave, Item>* put23(no_23<Chave, Item>* raiz, Chave chave, Item valor, bool &cresceu);
            int contaElementos(no_23<Chave, Item>*noRaiz);
            no_23<Chave, Item>*getNoByRank(no_23<Chave, Item>*noRaiz, int k);
            no_23<Chave, Item>*getMaiorNo(no_23<Chave, Item>*noRaiz);
            no_23<Chave, Item>*deleteNo(no_23<Chave, Item>*noRaiz, Chave chave, bool &noVazio);
            void deleteA23(no_23<Chave, Item>*noRaiz);
            void print23(no_23<Chave, Item>*noRaiz);
        public:
            void print();
            arvore23(string nomeDoTexto);
            ~arvore23();
            void insere (Chave chave, Item valor);
            Item devolve (Chave chave);
            void remove (Chave chave);
            int rank (Chave chave);
            Chave seleciona(int k);
    };

    template <class Chave, class Item>
    arvore23<Chave, Item>::arvore23(string nomeDoTexto){
        raiz = nullptr;

        fstream texto;
        string word;
        texto.open(nomeDoTexto);
        while (texto >> word){
            word = getOnlyWord(word);
            insere(word, 1);
        }
        texto.close();
    }

    template <class Chave, class Item>
    arvore23<Chave, Item>::~arvore23(){
        deleteA23(raiz);
    }

    template <class Chave, class Item>
    void arvore23<Chave, Item>::insere(Chave chave, Item valor){
        bool cresceu=false;
        raiz = put23(raiz, chave, valor, cresceu);
    }

    template <class Chave, class Item>
    Item arvore23<Chave, Item>::devolve(Chave chave){
        no_23<Chave, Item>* aux=raiz;
        while (aux != nullptr && aux->getNo(1)->getChave() != chave && !(aux->tresNo() && aux->getNo(2)->getChave() == chave)){
            if (aux->getNo(1)->getChave() > chave)
                aux = aux->getApt(1);
            else if(aux->tresNo() && aux->getNo(2)->getChave() < chave)
                aux = aux->getApt(3);
            else
                aux = aux->getApt(2);
        }
        if (aux != nullptr){
            if (aux->getNo(1)->getChave() == chave)
                return aux->getNo(1)->getValor();
            return aux->getNo(2)->getValor();
        }
        return 0;
    }

    template <class Chave, class Item>
    void arvore23<Chave, Item>::remove(Chave chave){
        bool vazio = false;
        raiz = deleteNo(raiz, chave, vazio);
    }

    template <class Chave, class Item>
    int arvore23<Chave, Item>::rank(Chave chave){
        no_23<Chave, Item>* aux=raiz;
        int k=0;
        while (aux != nullptr && aux->getNo(1)->getChave() != chave){
            if (aux->tresNo() && aux->getNo(2)->getChave() == chave)
                break;
            if (aux->getNo(1)->getChave() > chave)
                aux = aux->getApt(1);
            else if (aux->tresNo() && aux->getNo(2)->getChave() < chave){
                k += contaElementos(aux->getApt(1)) + contaElementos(aux->getApt(2));
                k += 2; //Raiz (nó 1 e nó 2)
                aux = aux->getApt(3);
            }
            else{
                k += contaElementos(aux->getApt(1));
                k += 1; //Raiz (nó 1)
                aux = aux->getApt(2);
            }
        }
        if (aux != nullptr){
            //Elemento existe na árvore
            k += contaElementos(aux->getApt(1));
            if (aux->tresNo() && aux->getNo(2)->getChave() == chave){
                k += contaElementos(aux->getApt(2));
                k += 1; //nó 1 da célula
            }
        }
        return k;
    }

    template <class Chave, class Item>
    Chave arvore23<Chave, Item>::seleciona(int k){
        no_23<Chave, Item>*aux = getNoByRank(raiz, k);
        if (aux != nullptr){
            if (rank(aux->getNo(1)->getChave()) == k)
                return aux->getNo(1)->getChave();
            return aux->getNo(2)->getChave();
        }
        return "";
    }

    //Funções privadas

    template <class Chave, class Item>
    no_23<Chave, Item> *arvore23<Chave, Item>::put23(no_23<Chave, Item>*noRaiz, Chave chave, Item valor, bool &cresceu){
        if (noRaiz == nullptr){
            noRaiz = new no_23<Chave, Item>;
            noRaiz->getNo(1)->setChave(chave);
            noRaiz->getNo(1)->setValor(valor);
            cresceu = true;
            return noRaiz;
        }
        if (noRaiz->noFolha()){
            //A raiz é folha
            if (noRaiz->getNo(1)->getChave() == chave || (noRaiz->tresNo() && noRaiz->getNo(2)->getChave() == chave)){
                //Caso Parituclar: Chave já existe na tabela
                if (noRaiz->getNo(1)->getChave() == chave)
                    noRaiz->getNo(1)->setValor(noRaiz->getNo(1)->getValor()+valor);
                else
                    noRaiz->getNo(2)->setValor(noRaiz->getNo(2)->getValor()+valor);
                cresceu = false;
                return noRaiz;
            }
            if (!noRaiz->tresNo()){
                //Insere na célula que é 2-Nó, transformando ela em uma 3-Nó
                if (noRaiz->getNo(1)->getChave() > chave){
                    noRaiz->getNo(2)->setChave(noRaiz->getNo(1)->getChave());
                    noRaiz->getNo(2)->setValor(noRaiz->getNo(1)->getValor());
                    noRaiz->getNo(1)->setChave(chave);
                    noRaiz->getNo(1)->setValor(valor);
                }
                else{
                    noRaiz->getNo(2)->setChave(chave);
                    noRaiz->getNo(2)->setValor(valor);
                }
                noRaiz->setNo23(3);
                cresceu = false;
                return noRaiz;
            }
            else{
                //Raiz é 3-Nós, ou seja, vai "explodir"
                no_23<Chave, Item> *meio=new no_23<Chave, Item>, *maior=new no_23<Chave, Item>;
                Chave menorChave, maiorChave, meioChave;
                Item menorValor, maiorValor, meioValor;
                if (chave > noRaiz->getNo(2)->getChave()){
                    maiorChave = chave;
                    maiorValor = valor;
                    meioChave = noRaiz->getNo(2)->getChave();
                    meioValor = noRaiz->getNo(2)->getValor();
                    menorChave = noRaiz->getNo(1)->getChave();
                    menorValor = noRaiz->getNo(1)->getValor();
                }
                else if (chave < noRaiz->getNo(1)->getChave()){
                    maiorChave = noRaiz->getNo(2)->getChave();
                    maiorValor = noRaiz->getNo(2)->getValor();
                    meioChave = noRaiz->getNo(1)->getChave();
                    meioValor = noRaiz->getNo(1)->getValor();
                    menorChave = chave;
                    menorValor = valor;
                }
                else{
                    maiorChave = noRaiz->getNo(2)->getChave();
                    maiorValor = noRaiz->getNo(2)->getValor();
                    meioChave = chave;
                    meioValor = valor;
                    menorChave = noRaiz->getNo(1)->getChave();
                    menorValor = noRaiz->getNo(1)->getValor();
                }
                meio->getNo(1)->setChave(meioChave);
                meio->getNo(1)->setValor(meioValor);
                meio->setApt(noRaiz, 1);
                meio->setApt(maior, 2);
                maior->getNo(1)->setChave(maiorChave);
                maior->getNo(1)->setValor(maiorValor);
                noRaiz->getNo(1)->setChave(menorChave);
                noRaiz->getNo(1)->setValor(menorValor);
                noRaiz->setNo23(2);
                cresceu = true;
                return meio;
            }
        }
        else{
            //A raiz não é folha
            if (noRaiz->getNo(1)->getChave() == chave || (noRaiz->tresNo() && noRaiz->getNo(2)->getChave() == chave)){
                //Caso Parituclar: Chave já existe na tabela
                if (noRaiz->getNo(1)->getChave() == chave)
                    noRaiz->getNo(1)->setValor(noRaiz->getNo(1)->getValor()+valor);
                else
                    noRaiz->getNo(2)->setValor(noRaiz->getNo(2)->getValor()+valor);
                cresceu = false;
                return noRaiz;
            }
            else if (noRaiz->getNo(1)->getChave() > chave){
                //Chama recursão para a esquerda do Nó
                no_23<Chave, Item> *aux = put23(noRaiz->getApt(1), chave, valor, cresceu);
                if (!cresceu)
                    return noRaiz;
                else{
                    //Cresceu
                    if (!noRaiz->tresNo()){
                        //Cresceu e vai transformar a raiz que é 2-Nó em uma 3-Nó
                        noRaiz->getNo(2)->setChave(noRaiz->getNo(1)->getChave());
                        noRaiz->getNo(2)->setValor(noRaiz->getNo(1)->getValor());
                        noRaiz->getNo(1)->setChave(aux->getNo(1)->getChave());
                        noRaiz->getNo(1)->setValor(aux->getNo(1)->getValor());
                        noRaiz->setApt(noRaiz->getApt(2), 3);
                        noRaiz->setApt(aux->getApt(2), 2);
                        noRaiz->setApt(aux->getApt(1), 1);
                        delete aux;
                        noRaiz->setNo23(3);
                        cresceu = false;
                        return noRaiz;
                    }
                    else{
                        //Cresceu e a raiz é 3-Nó, ou seja, vai "explodir"
                        no_23<Chave, Item> *maior = new no_23<Chave, Item>;
                        maior->getNo(1)->setChave(noRaiz->getNo(2)->getChave());
                        maior->getNo(1)->setValor(noRaiz->getNo(2)->getValor());
                        maior->setApt(noRaiz->getApt(2), 1);
                        maior->setApt(noRaiz->getApt(3), 2);
                        noRaiz->setApt(aux, 1);
                        noRaiz->setApt(maior, 2);
                        noRaiz->setNo23(2);
                        cresceu = true;
                        return noRaiz;
                    }
                }
            }
            else if (noRaiz->tresNo() && noRaiz->getNo(2)->getChave() < chave){
                //Chama a recursão para a direita do Nó
                no_23<Chave, Item> *aux = put23(noRaiz->getApt(3), chave, valor, cresceu);
                if (!cresceu)
                    return noRaiz;
                else{
                    //cresceu e a raiz é 3-Nó, ou seja, vai "explodir"
                    no_23<Chave, Item> *menor = new no_23<Chave, Item>;
                    menor->getNo(1)->setChave(noRaiz->getNo(1)->getChave());
                    menor->getNo(1)->setValor(noRaiz->getNo(1)->getValor());
                    menor->setApt(noRaiz->getApt(1), 1);
                    menor->setApt(noRaiz->getApt(2), 2);
                    noRaiz->getNo(1)->setChave(noRaiz->getNo(2)->getChave());
                    noRaiz->getNo(1)->setValor(noRaiz->getNo(2)->getValor());
                    noRaiz->setApt(menor, 1);
                    noRaiz->setApt(aux, 2);
                    noRaiz->setNo23(2);
                    cresceu = true;
                    return noRaiz;
                }
            }
            else{
                //Chama a recursão para o meio do Nó
                no_23<Chave, Item> *aux = put23(noRaiz->getApt(2), chave, valor, cresceu);
                if (!cresceu)
                    return noRaiz;
                else{
                    //cresceu
                    if (!noRaiz->tresNo()){
                        //cresceu e vai transformar a raiz que é 2-Nó em uma 3-Nó
                        noRaiz->getNo(2)->setChave(aux->getNo(1)->getChave());
                        noRaiz->getNo(2)->setValor(aux->getNo(1)->getValor());
                        noRaiz->setApt(aux->getApt(1), 2);
                        noRaiz->setApt(aux->getApt(2), 3);
                        delete aux;
                        noRaiz->setNo23(3);
                        cresceu = false;
                        return noRaiz;
                    }
                    else{
                        //cresceu e a raiz é 3-Nó, ou seja, vai "explodir"
                        no_23<Chave, Item> *maior = new no_23<Chave, Item>;
                        maior->getNo(1)->setChave(noRaiz->getNo(2)->getChave());
                        maior->getNo(1)->setValor(noRaiz->getNo(2)->getValor());
                        maior->setApt(noRaiz->getApt(3), 2);
                        maior->setApt(aux->getApt(2), 1);
                        noRaiz->setApt(aux->getApt(1), 2);
                        noRaiz->setNo23(2);
                        aux->setApt(noRaiz, 1);
                        aux->setApt(maior, 2);
                        cresceu = true;
                        return aux;
                    }
                }
            }
        }   
    }

    /*Conta o número de elementos abaixo do "noRaiz", com o próprio nó incluso na conta*/
    template <class Chave, class Item>
    int arvore23<Chave, Item>::contaElementos(no_23<Chave, Item>*noRaiz){
        if (noRaiz != nullptr){
            int esq, dir, meio;
            meio = dir = esq = 0;
            esq = contaElementos(noRaiz->getApt(1));
            meio = contaElementos(noRaiz->getApt(2));
            if (noRaiz->tresNo()){
                dir = contaElementos(noRaiz->getApt(3));
                return (esq + meio + dir + 2);
            }
            return (esq + meio + 1);
        }
        return 0;
    }

    /*Busca o nó na árvore através do rank*/
    template <class Chave, class Item>
    no_23<Chave, Item>* arvore23<Chave, Item>::getNoByRank(no_23<Chave, Item>*noRaiz, int k){
        if (noRaiz == nullptr)
            return nullptr;
        int rankAtual, rankAtual2;
        rankAtual = rank(noRaiz->getNo(1)->getChave());
        if (noRaiz->tresNo())
            rankAtual2 = rank(noRaiz->getNo(2)->getChave());

        if (rankAtual > k)
            return getNoByRank(noRaiz->getApt(1), k);
        else if (noRaiz->tresNo() && rankAtual2 < k)
            return getNoByRank(noRaiz->getApt(3), k);
        else if (rankAtual < k && rankAtual2 != k)
            return getNoByRank(noRaiz->getApt(2), k);

        return noRaiz;
    }

    template <class Chave, class Item>
    no_23<Chave, Item>* arvore23<Chave, Item>::getMaiorNo(no_23<Chave, Item>*noRaiz){
        /*Retorna o maior nó da árvore de raiz "noRaiz"*/
        if (noRaiz->tresNo() && noRaiz->getApt(3) != nullptr)
            return getMaiorNo(noRaiz->getApt(3));
        else if (!noRaiz->tresNo() && noRaiz->getApt(2) != nullptr)
            return getMaiorNo(noRaiz->getApt(2));
        return noRaiz;
    }

    template <class Chave, class Item>
    no_23<Chave, Item>* arvore23<Chave, Item>::deleteNo(no_23<Chave, Item>*noRaiz, Chave chave, bool &noVazio){
        if (noRaiz == nullptr)
            return nullptr;
        if (noRaiz->noFolha()){
            if(noRaiz->tresNo()){
                //O Nó é 3-Nó, ou seja, só remove a chave
                if (noRaiz->getNo(1)->getChave() == chave){
                    noRaiz->getNo(1)->setChave(noRaiz->getNo(2)->getChave());
                    noRaiz->getNo(1)->setValor(noRaiz->getNo(2)->getValor());
                }
                noRaiz->setNo23(2);
                noVazio = false;
            }
            else{
                //O Nó é 2-Nó
                noVazio = true;
                if(noRaiz == raiz){
                    delete raiz;
                    return nullptr;
                }
            }
        }
        else{
            if (noRaiz->getNo(1)->getChave() >= chave){
                no_23<Chave, Item>* aux;
                if (noRaiz->getNo(1)->getChave() == chave){
                    //Não é folha e vai remover o maior da sub Árovre esquerda, ou seja, precisa fazer as checagens
                    aux = getMaiorNo(noRaiz->getApt(1));
                    if (!aux->tresNo()){
                        noRaiz->getNo(1)->setChave(aux->getNo(1)->getChave());
                        noRaiz->getNo(1)->setValor(aux->getNo(1)->getValor());
                        noRaiz->setApt(deleteNo(noRaiz->getApt(1), aux->getNo(1)->getChave(), noVazio), 1);
                    }
                    else{
                        noRaiz->getNo(1)->setChave(aux->getNo(2)->getChave());
                        noRaiz->getNo(1)->setValor(aux->getNo(2)->getValor());
                        noRaiz->setApt(deleteNo(noRaiz->getApt(1), aux->getNo(2)->getChave(), noVazio), 1);
                    }
                    if (!noVazio)
                        return noRaiz; 
                }
                else{
                    //Recursão para a esquerda do nó
                    aux = deleteNo(noRaiz->getApt(1), chave, noVazio);
                    if (!noVazio){
                        noRaiz->setApt(aux, 1);
                        return noRaiz;
                    }
                }
                //Nó filho está vazio
                no_23<Chave, Item>*filho = noRaiz->getApt(1), *irmao = noRaiz->getApt(2);
                if (irmao->tresNo()){
                    //O irmão do meio tem para emprestar
                    filho->getNo(1)->setChave(noRaiz->getNo(1)->getChave());
                    filho->getNo(1)->setValor(noRaiz->getNo(1)->getValor());
                    noRaiz->getNo(1)->setChave(irmao->getNo(1)->getChave());
                    noRaiz->getNo(1)->setValor(irmao->getNo(1)->getValor());
                    if (filho->getApt(2) != nullptr){
                        filho->setApt(filho->getApt(2), 1);
                    }
                    filho->setApt(irmao->getApt(1), 2);
                    irmao->setApt(irmao->getApt(2), 1);
                    irmao->setApt(irmao->getApt(3), 2);
                    irmao->getNo(1)->setChave(irmao->getNo(2)->getChave());
                    irmao->getNo(1)->setValor(irmao->getNo(2)->getValor());
                    irmao->setNo23(2);
                    noVazio = false;
                }
                else{
                    //O irmão não tem, ou seja, Monta um 3-Nó com o pai e o irmão
                    irmao->getNo(2)->setChave(irmao->getNo(1)->getChave());
                    irmao->getNo(2)->setValor(irmao->getNo(1)->getValor());
                    irmao->getNo(1)->setChave(noRaiz->getNo(1)->getChave());
                    irmao->getNo(1)->setValor(noRaiz->getNo(1)->getValor());
                    irmao->setNo23(3);
                    irmao->setApt(irmao->getApt(2), 3);
                    irmao->setApt(irmao->getApt(1), 2);
                    if (filho->getApt(2) != nullptr)
                        irmao->setApt(filho->getApt(2), 1);
                    else
                        irmao->setApt(filho->getApt(1), 1);
                    delete filho;
                    noRaiz->setApt(nullptr, 1);
                    if (noRaiz->tresNo()){
                        //O pai é 3-Nó, ou seja, o pai vira 2-Nó
                        noRaiz->getNo(1)->setChave(noRaiz->getNo(2)->getChave());
                        noRaiz->getNo(1)->setValor(noRaiz->getNo(2)->getValor());
                        noRaiz->setApt(noRaiz->getApt(2), 1);
                        noRaiz->setApt(noRaiz->getApt(3), 2);
                        noRaiz->setApt(nullptr, 3);
                        noRaiz->setNo23(2);
                        noVazio = false;
                    }
                    else{
                        //O pai é 2-Nó, ou seja, o pai fica vazio
                        noVazio = true;
                        if (noRaiz == raiz){
                            raiz = noRaiz->getApt(2);
                            delete noRaiz;
                            return raiz;
                        }
                    }
                }

            }
            else if (noRaiz->tresNo() && noRaiz->getNo(2)->getChave() < chave){
                //Recursão para a direita do nó
                no_23<Chave, Item>* aux = deleteNo(noRaiz->getApt(3), chave, noVazio);
                if (!noVazio){
                    noRaiz->setApt(aux, 3);
                    return noRaiz;
                }
                else{
                    no_23<Chave, Item>*filho = noRaiz->getApt(3), *irmao = noRaiz->getApt(2);
                    if (irmao->tresNo()){
                        //O irmão do meio tem para emprestar
                        filho->getNo(1)->setChave(noRaiz->getNo(2)->getChave());
                        filho->getNo(1)->setValor(noRaiz->getNo(2)->getValor());
                        noRaiz->getNo(2)->setChave(irmao->getNo(2)->getChave());
                        noRaiz->getNo(2)->setValor(irmao->getNo(2)->getValor());
                        if (filho->getApt(1) != nullptr)
                            filho->setApt(filho->getApt(1), 2);
                        filho->setApt(irmao->getApt(3), 1);
                        irmao->setNo23(2);
                        noVazio = false;
                    }
                    else{
                        //O irmão não tem, ou seja, Monta um 3-Nó com o pai e o irmão
                        irmao->getNo(2)->setChave(noRaiz->getNo(2)->getChave());
                        irmao->getNo(2)->setValor(noRaiz->getNo(2)->getValor());
                        irmao->setNo23(3);
                        if (filho->getApt(1) != nullptr)
                            irmao->setApt(filho->getApt(1), 3);
                        else
                            irmao->setApt(filho->getApt(2), 3);
                        delete filho;
                        noRaiz->setApt(nullptr, 3);
                        noRaiz->setNo23(2);
                        noVazio = false;
                    }
                }
            }
            else{
                no_23<Chave, Item>* aux;
                if ((noRaiz->tresNo() && noRaiz->getNo(2)->getChave() == chave)){
                    //Não é folha e vai remover o maior da sub Árovre do meio, ou seja, precisa fazer as checagens
                    aux = getMaiorNo(noRaiz->getApt(2));
                    if (!aux->tresNo()){
                        noRaiz->getNo(2)->setChave(aux->getNo(1)->getChave());
                        noRaiz->getNo(2)->setValor(aux->getNo(1)->getValor());
                        noRaiz->setApt(deleteNo(noRaiz->getApt(2), aux->getNo(1)->getChave(), noVazio), 2);
                    }
                    else{
                        noRaiz->getNo(2)->setChave(aux->getNo(2)->getChave());
                        noRaiz->getNo(2)->setValor(aux->getNo(2)->getValor());
                        noRaiz->setApt(deleteNo(noRaiz->getApt(2), aux->getNo(2)->getChave(), noVazio), 2);
                    }
                    if (!noVazio)
                        return noRaiz;
                }
                else{
                    //Recursão para o meio do nó
                    aux = deleteNo(noRaiz->getApt(2), chave, noVazio);
                    if (!noVazio){
                        noRaiz->setApt(aux, 2);
                        return noRaiz;
                    }
                }
                //Nó filho está vazio
                no_23<Chave, Item>*filho = noRaiz->getApt(2), *irmaoEsq = noRaiz->getApt(1), *irmaoDir = noRaiz->getApt(3);
                if (irmaoEsq->tresNo()){
                    //O irmão esquerdo tem para emprestar
                    filho->getNo(1)->setChave(noRaiz->getNo(1)->getChave());
                    filho->getNo(1)->setValor(noRaiz->getNo(1)->getValor());
                    noRaiz->getNo(1)->setChave(irmaoEsq->getNo(2)->getChave());
                    noRaiz->getNo(1)->setValor(irmaoEsq->getNo(2)->getValor());
                    if (filho->getApt(1) != nullptr)
                        filho->setApt(filho->getApt(1), 2);
                    filho->setApt(irmaoEsq->getApt(3), 1);
                    irmaoEsq->setNo23(2);
                    noVazio = false;
                }
                else if (noRaiz->tresNo() && irmaoDir->tresNo()){
                    //O irmão direito tem pra emprestar
                    filho->getNo(1)->setChave(noRaiz->getNo(2)->getChave());
                    filho->getNo(1)->setValor(noRaiz->getNo(2)->getValor());
                    noRaiz->getNo(2)->setChave(irmaoDir->getNo(1)->getChave());
                    noRaiz->getNo(2)->setValor(irmaoDir->getNo(1)->getValor());
                    irmaoDir->getNo(1)->setChave(irmaoDir->getNo(2)->getChave());
                    irmaoDir->getNo(1)->setValor(irmaoDir->getNo(2)->getValor());
                    if (filho->getApt(2) != nullptr)
                        filho->setApt(filho->getApt(2), 1);
                    filho->setApt(irmaoDir->getApt(1), 2);
                    irmaoDir->setApt(irmaoDir->getApt(2), 1);
                    irmaoDir->setApt(irmaoDir->getApt(3), 2);
                    irmaoDir->setNo23(2);
                    noVazio = false;
                }

                else{
                    //O irmão não tem, ou seja, Monta um 3-Nó com o pai e o irmão
                    irmaoEsq->getNo(2)->setChave(noRaiz->getNo(1)->getChave());
                    irmaoEsq->getNo(2)->setValor(noRaiz->getNo(1)->getValor());
                    irmaoEsq->setNo23(3);
                    if (filho->getApt(1) != nullptr)
                        irmaoEsq->setApt(filho->getApt(1), 3);
                    else
                        irmaoEsq->setApt(filho->getApt(2), 3);
                    delete filho;
                    noRaiz->setApt(nullptr, 2);
                    if (noRaiz->tresNo()){
                        //O pai é 3-Nó, ou seja, o pai vira 2-Nó
                        noRaiz->getNo(1)->setChave(noRaiz->getNo(2)->getChave());
                        noRaiz->getNo(1)->setValor(noRaiz->getNo(2)->getValor());
                        noRaiz->setApt(noRaiz->getApt(3), 2);
                        noRaiz->setApt(nullptr, 3);
                        noRaiz->setNo23(2);
                        noVazio = false;
                    }
                    else{
                        //O pai é 2-Nó, ou seja, o pai fica vazio
                        noVazio = true;
                        if (noRaiz == raiz){
                            raiz = noRaiz->getApt(1);
                            delete noRaiz;
                            return raiz;
                        }
                    }
                }
            }
        }
        return noRaiz;
    }

    template <class Chave, class Item>
    void arvore23<Chave, Item>::deleteA23(no_23<Chave, Item>*noRaiz){
        if (noRaiz != nullptr){
            deleteA23(noRaiz->getApt(1));
            if (noRaiz->tresNo())
                deleteA23(noRaiz->getApt(3));
            deleteA23(noRaiz->getApt(2));
            delete noRaiz;
        }
    }

    template <class Chave, class Item>
    void arvore23<Chave, Item>::print(){
        cout<<"\n##PRINT ARVORE##\n";
        print23(raiz);
        cout<<"\n";
    }

    template <class Chave, class Item>
    void arvore23<Chave, Item>::print23(no_23<Chave, Item>*noRaiz){
        if (noRaiz != nullptr){
            print23(noRaiz->getApt(1));
            cout<<noRaiz->getNo(1)->getChave()<<" | "<<noRaiz->getNo(1)->getValor()<<endl;
            print23(noRaiz->getApt(2));
            if (noRaiz->tresNo()){
                cout<<noRaiz->getNo(2)->getChave()<<" | "<<noRaiz->getNo(2)->getValor()<<endl;
                print23(noRaiz->getApt(3));
            }
        }
    }

#endif