#include "util.hpp"
using namespace std;

/*Implementação de funções auxiliares*/

string getOnlyWord(string input){
    int len;
    //Remove do início
    for(len=0; isSymbol(input[len]); len++);
    input.erase(input.begin(), input.begin()+len);
    //Remove do fim
    for (len=input.length()-1; isSymbol(input[len]); len--);
    if (len != (int)(input.length()-1)){
        input.erase(input.begin()+(len+1), input.end());
    }
    return input;
}

bool isSymbol(char input){
    bool ret;
    switch (input){
    case ' ':
    case '_':
    case '\'':
    case '"':
    case '-':
    case '~':
    case '^':
    case '!':
    case '@':
    case '#':
    case '$':
    case '%':
    case '&':
    case '*':
    case '(':
    case ')':
    case '[':
    case ']':
    case '{':
    case '}':
    case '?':
    case '\\':
    case '/':
    case '.':
    case ',':
    case ':':
    case ';':
    case '<':
    case '>':
        ret = true;
        break;
    
    default:
        ret = false;
        break;
    }
    return ret;
}