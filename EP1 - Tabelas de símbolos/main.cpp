#include <iostream>
#include <sstream>
#include <fstream>
#include <time.h>
/*-------------------------------------------------------------*/ 
/* Importando as diferentes tabelas */
#include "./src/2-3.hpp"
#include "./src/abb.hpp"
#include "./src/hashTable.hpp"
#include "./src/linkedList.hpp"
#include "./src/rubroNegra.hpp"
#include "./src/treap.hpp"
#include "./src/vector.hpp"
#include "./src/util.hpp"

using namespace std;

/*-------------------------------------------------------------*/ 
/* alguns macros/abreviaturas uteis */
#define ERROR(msg)   fprintf(stderr,"ERROR: %s\n", #msg)
#define WARNING(msg) fprintf(stdout,"WARNING: %s\n", #msg)
#define PROMPT      cout << ">>> ";

#define INSERE_ST     "insereST"
#define MIN_ST        "minST"
#define DELMIN_ST     "delminST"
#define GET_ST        "getST"
#define RANK_ST       "rankST"
#define DELETE_ST     "deleteST"
#define SELECT_ST     "selectST"
#define PRINT_ST      "mostraST"
#define DEL_RANDOM_ST "deleteChavesST"
#define CLEAN_ST      "limpaST"
/*-------------------------------------------------------------*/ 
/*Declaração do tipo "Chave" e "Item"*/
typedef string Chave;
typedef int Item;
/*-------------------------------------------------------------*/ 
/* prototipo das funções */

template <class ST>
void testeOperacoes(ST st);

static void mostreUso (char *nomePrograma);

static void mostraOperacoes();

template <class ST>
void debug(ST st, int max);

/*Prototipo das funções de teste automático*/

/*
* testeRapido()
* Faz a inserção de 10 palavras aleatórias e faz 2 testes:
* 1º: Devolve os valores das chaves (Usando as funções seleciona e devolve);
* 2º: Printa o mínimo e deleta (Usando as função seleciona e remove).
*/
template <class ST>
void testeRapido(ST st);

/*
* testaTudo();
* Chama as funções printST() e CleanST() em sequência automaticamente.
*/
template <class ST>
void testaTudo(ST st);

/*
* printST()
* Printa em ordem crescente as palavras contidas na tabela.
*/
template <class ST>
void printST(ST st);

/*
* deleteRandomKeys()
* Deleta randomicamente "numKeys" chaves da tabela.
*/
template <class ST>
void deleteRandomKeys(ST st, int numKeys);

/*
* CleanST()
* Remove todos os elementos da tabela.
*/
template <class ST>
void cleanST(ST st);

int main (int argc, char *argv[]){
    int uso;
    cout<<"\nQual tipo de teste você deseja fazer?";
    cout<<"\n1 - Teste manual com leitura de arquivo";
    cout<<"\n2 - Teste rapido sem leitura de arquivo(Inserção de algumas palavras aleatórias)";
    cout<<"\n3 - Teste automatizado de print e delete com leitura de arquivo\n";
    PROMPT
    cin>>uso;

    if (argc < 3 && uso != 2){
        cout<<endl;
        mostreUso(argv[0]);
    }

    else{
        fstream arqTexto;
        srand(2020);

        if (uso != 2){
            //Testa se o arquivo é um arquivo válido
            arqTexto.open(argv[1]);

            if (arqTexto.fail()) {
                cout << "ERRO: arquivo" << argv[1] << "nao pode ser aberto.\n";
                exit(EXIT_FAILURE);
            }

            arqTexto.close();
        }

        string tipo, nome_arquivo;
        clock_t start, end; 
        double elapsed = 0;

        if (uso == 2)
            tipo = argv[1];
        else{
            tipo = argv[2];
            nome_arquivo = argv[1];
        }

        if (tipo == "VD"){
            start = clock(); 
            vetorDes<Chave, Item> *st = new vetorDes<Chave, Item>(nome_arquivo);
            end = clock();

            /* calcule o tempo */
            elapsed = ((double) (end - start)) / CLOCKS_PER_SEC;
            cout << "arquivo lido e ST construida em " << elapsed << " segundos\n";

            if (uso == 1)
                testeOperacoes(st);
            else if (uso == 2)
                testeRapido(st);
            else
                testaTudo(st);
            delete st;
        }
        else if (tipo == "VO"){
            start = clock(); 
            vetorOrd<Chave, Item> *st = new vetorOrd<Chave, Item>(nome_arquivo);
            end = clock();

            /* calcule o tempo */
            elapsed = ((double) (end - start)) / CLOCKS_PER_SEC;
            cout << "arquivo lido e ST construida em " << elapsed << " segundos\n";

            if (uso == 1)
                testeOperacoes(st);
            else if (uso == 2)
                testeRapido(st);
            else
                testaTudo(st);
            delete st;
        }
        else if (tipo == "LD"){
            start = clock(); 
            listaDes<Chave, Item> *st = new listaDes<Chave, Item>(nome_arquivo);
            end = clock();

            /* calcule o tempo */
            elapsed = ((double) (end - start)) / CLOCKS_PER_SEC;
            cout << "arquivo lido e ST construida em " << elapsed << " segundos\n";

            if (uso == 1)
                testeOperacoes(st);
            else if (uso == 2)
                testeRapido(st);
            else
                testaTudo(st);
            delete st;
        }
        else if (tipo == "LO"){
            start = clock(); 
            listaOrd<string, int> *st = new listaOrd<string, int>(nome_arquivo);
            end = clock();

            /* calcule o tempo */
            elapsed = ((double) (end - start)) / CLOCKS_PER_SEC;
            cout << "arquivo lido e ST construida em " << elapsed << " segundos\n";

            if (uso == 1)
                testeOperacoes(st);
            else if (uso == 2)
                testeRapido(st);
            else
                testaTudo(st);
            delete st;
        }
        else if (tipo == "AB"){
            start = clock(); 
            arvoreBin<Chave, Item> *st = new arvoreBin<Chave, Item>(nome_arquivo);
            end = clock();

            /* calcule o tempo */
            elapsed = ((double) (end - start)) / CLOCKS_PER_SEC;
            cout << "arquivo lido e ST construida em " << elapsed << " segundos\n";

            if (uso == 1)
                testeOperacoes(st);
            else if (uso == 2)
                testeRapido(st);
            else
                testaTudo(st);
            delete st;
        }
        else if (tipo == "TR"){
            start = clock(); 
            treap<string, int> *st = new treap<string, int>(nome_arquivo);
            end = clock();

            /* calcule o tempo */
            elapsed = ((double) (end - start)) / CLOCKS_PER_SEC;
            cout << "arquivo lido e ST construida em " << elapsed << " segundos\n";

            if (uso == 1)
                testeOperacoes(st);
            else if (uso == 2)
                testeRapido(st);
            else
                testaTudo(st);
            delete st;
        }
        else if (tipo == "A23"){
            start = clock(); 
            arvore23<string, int> *st = new arvore23<string, int>(nome_arquivo);
            end = clock();

            /* calcule o tempo */
            elapsed = ((double) (end - start)) / CLOCKS_PER_SEC;
            cout << "arquivo lido e ST construida em " << elapsed << " segundos\n";

            if (uso == 1)
                testeOperacoes(st);
            else if (uso == 2)
                testeRapido(st);
            else
                testaTudo(st);
            delete st;
        }
        else if (tipo == "RN"){
            start = clock(); 
            arvoreRN<string, int> *st = new arvoreRN<string, int>(nome_arquivo);
            end = clock();

            /* calcule o tempo */
            elapsed = ((double) (end - start)) / CLOCKS_PER_SEC;
            cout << "arquivo lido e ST construida em " << elapsed << " segundos\n";

            if (uso == 1)
                testeOperacoes(st);
            else if (uso == 2)
                testeRapido(st);
            else
                testaTudo(st);
            delete st;
        }
        else if (tipo == "HS"){
            start = clock(); 
            hashTable<string, int> *st = new hashTable<string, int>(nome_arquivo);
            end = clock();

            /* calcule o tempo */
            elapsed = ((double) (end - start)) / CLOCKS_PER_SEC;
            cout << "arquivo lido e ST construida em " << elapsed << " segundos\n";
            if (uso == 1)
                testeOperacoes(st);
            else if (uso == 2)
                testeRapido(st);
            else
                testaTudo(st);
            delete st;
        }
        else {
            cout << "A estrutura " << tipo << " não é válida";
            exit(EXIT_FAILURE);
        }
    }
    return EXIT_SUCCESS;
}

/*-----------------------------------------------------------*/
/* Implementação das funções*/
template <class ST>
void testeOperacoes(ST st){
    string linha, operacao;
    
    /* mostre uso */
    mostraOperacoes();
    while (getline(cin, linha)){
        /* pegue operacao a ser testada */
        stringstream X(linha); //Transforma a string "linha" em uma stream "X" 
        getline(X, operacao, ' ');
        if (operacao == ""){
             ERROR(operacao esperada);
        }
        /*---------------------------------*/
        else if (operacao == MIN_ST){
            string key = st->seleciona(0);
            if (key == "")
                cout << "ST vazia\n";
            else
                cout << key << "\n";
        }
        /*---------------------------------*/
        else if (operacao == DELMIN_ST){
            string key = st->seleciona(0);
            if (key == "")
                cout << "ST já está vazia\n";
            else{
                st->remove(key);
                cout << "\"" << key << "\" foi removida\n";
            }
        }
        /*---------------------------------*/
        else if (operacao == PRINT_ST){
            printST(st);
        }
        /*---------------------------------*/
        else if (operacao == CLEAN_ST){
            cleanST(st);
        }
        /*---------------------------------*/
        else if (operacao == DEL_RANDOM_ST){
            string keys;
            int numKeys;
            getline(X, keys, ' ');
            if (keys == "" || keys == "0"){
                ERROR(operacao necessita um valor);
            }
            numKeys = stoi(keys);
            deleteRandomKeys(st, numKeys);
        }
        /*---------------------------------*/
        else{
            /* operação necessita de argumento key */
            string key;
            getline(X, key, ' ');
            if (key == ""){
                ERROR(operacao necessita uma palavra);
            }
            else{
                /*---------------------------------*/
                if (operacao == GET_ST){
                    int frequencia = st->devolve(key); /* consulte a ST */
                    /* mostre o resultado da consulta */
                    if (frequencia == 0)
                        cout << key << ": 0\n";
                    else 
                        cout << key << ": " << frequencia << "\n";
                }
                /*---------------------------------*/
                else if (operacao == RANK_ST){
                    int r = st->rank(key);
                    cout << r << "\n";
                }
                /*---------------------------------*/
                else if (operacao == DELETE_ST){
                    st->remove(key);
                    cout << "\"" << key << "\" foi removida\n";
                }
                /*---------------------------------*/
                else if (operacao == SELECT_ST){
                    int pos = stoi(key);
                    string chave = st->seleciona(pos);
                    cout << "Posição " << key << " = " << chave << "\n";
                }
                /*---------------------------------*/
                else if (operacao == INSERE_ST){
                    string valor;
                    getline(X, valor, ' ');
                    if (valor == ""){
                        ERROR(operacao necessita de um valor);
                    }
                    else{
                        int val = stoi(valor);
                        st->insere(key, val);
                        cout << "\"" << key << "\" foi inserida com o valor "<< val << "\n";
                    }
                }
                /*---------------------------------*/
                else{
                    ERROR(operacao nao reconhecida);
                }
            }
        }
        mostraOperacoes();
    }
    cout << "\n";
}

static void mostraOperacoes(){
    cout << "Possiveis operacoes do teste interativo:\n";
    cout << "insereST<chave> <valor>, minST, delminST, getST <chave>, rankST <chave>, deleteST <chave>, selectST <int>\n";
    cout<<"Possiveis operacoes de teste automatizado\n";
    cout<<"mostraST, deleteChavesST<num_de_chaves>, limpaST\n";
    cout << "CRTL-D para encerrar.\n";
    PROMPT;
}

static void mostreUso (char *nomePrograma){
    cout << "Uso \n"
         << "Para os testes 1 e 3:\n" 
	     << "prompt> " << nomePrograma << " nome-arquivo tipo-tabela\n"
         << "Para o teste 2:\n" 
	     << "prompt> " << nomePrograma << " tipo-tabela\n"
	     << "    nome-arquivo = nome do arquivo com o texto\n"
         << "    tipo-tabela  = sigla de estrutura que deve ser usada\n";
    exit(EXIT_FAILURE);
}   

template <class ST>
void debug(ST st, int max){
    for (int i=0; i < max; i++){
        string key = st->seleciona(i);
        int frequencia = st->devolve(key);
        cout << key << ": " << frequencia << "\n";
    }
    cout<<"\n###############################\n";
}

/* Implementação das funções de teste automatizado*/

template <class ST>
void testeRapido(ST st){
    clock_t start, end; 
    double media = 0;
    start = clock();
    st->insere("Banana", 4);
    debug(st, 1);
    st->insere("Arma", 3);
    debug(st, 2);
    st->insere("Braço", 10);
    debug(st, 3);
    st->insere("Tesoura", 20);
    debug(st, 4);
    st->insere("Geleia", 5);
    debug(st, 5);
    st->insere("Conglomerado", 15);
    debug(st, 6);
    st->insere("Abacaxi", 2);
    debug(st, 7);
    st->insere("Xícara", 9);
    debug(st, 8);
    st->insere("Lâmpada", 25);
    debug(st, 9);
    st->insere("Esponja", 100);
    end = clock();
    media = ((double) (end - start))/CLOCKS_PER_SEC;
    cout << "\nTempo gasto: "<< media<< " segundos\n";

    cout<< "\nTeste 1: Devolve os valores das chaves\n";
    start = end = 0;
    start = clock();
    for (int i=0; i < 10; i++){
        string key = st->seleciona(i);
        int frequencia = st->devolve(key);
        cout << key << ": " << frequencia << "\n";
    }
    end = clock();
    media = ((double) (end - start))/CLOCKS_PER_SEC;
    cout << "\nTempo gasto: "<< media<< " segundos\n";

    cout<< "\nTeste 2: Printa o mínimo e deleta\n";
    start = end = 0;
    start = clock();
    for (int i = 0; i < 10; i++){
        string key = st->seleciona(0);
        if (key == ""){
            cout << "ST vazia\n";
            break;
        }
        cout << key << "\n";
        st->remove(key);
        cout << "\"" << key << "\" foi removida\n";
    }
    end = clock();
    media = ((double) (end - start))/CLOCKS_PER_SEC;
    cout << "\nTempo gasto: "<< media<< " segundos\n";
}

template <class ST>
void testaTudo(ST st){
    char c;
    cout<< "\nTeste 1: Print da tabela inteira em ordem crescente de chave\n";
    cout<<"Digite qualquer tecla para começar...";
    c = getchar();
    if (c == '\n')
        c = getchar();
    printST(st);
    cout<< "\nTeste 2: Delete de todas as chaves da tabela\n";
    cout<<"Digite qualquer tecla para começar...";
    c = getchar();
    cleanST(st);
}

template <class ST>
void printST(ST st){
    string key;
    clock_t start, end;
    double media = 0; 
    if (st->seleciona(0) == ""){
        cout<<"\nST Vazia.\n";
        return;
    }
    start = clock();
    for (int i=0; (key = st->seleciona(i)) != ""; i++){
        int frequencia = st->devolve(key);
        cout << key << ": " << frequencia << "\n";
    }
    end = clock();
    media = ((double) (end - start))/CLOCKS_PER_SEC;
    cout << "\nTempo gasto: "<< media<< " segundos\n";
}

template <class ST>
void deleteRandomKeys(ST st, int numKeys){
    string key;
    clock_t start, end;
    double media = 0; 
    int keyAtual, rangeMax;
    if (st->seleciona(numKeys) == ""){
        cout<<"\nST não tem elementos suficientes para serem removidos. Tente menos elementos\n";
        return;
    }
    rangeMax = 5000;
    start = clock();
    for (int i = 0; i < numKeys; i++){
        keyAtual = rand()%rangeMax;
        if ( (key = st->seleciona(keyAtual)) == ""){
            i--;
            rangeMax = rangeMax/2;
        }
        else{
            cout<< endl << i <<" |Removendo: "<< key <<"\n";
            cout.flush();
            st->remove(key);
            cout<< key <<" Removida.\n";
        }
    }
    end = clock();
    media = ((double) (end - start))/CLOCKS_PER_SEC;
    cout << "\nTempo gasto: "<< media<< " segundos\n";
}

template <class ST>
void cleanST(ST st){
    string key;
    clock_t start, end;
    double media = 0; 
    if (st->seleciona(0) == ""){
        cout<<"\nST Vazia.\n";
        return;
    }
    start = clock();
    while ((key = st->seleciona(0)) != ""){
        cout<<"\nRemovendo: "<< key <<"\n";
        cout.flush();
        st->remove(key);
        cout<< key <<" Removida.\n";
    }   
    end = clock();
    media = ((double) (end - start))/CLOCKS_PER_SEC;
    cout << "\nTempo gasto: "<< media<< " segundos\n";
}