#include "util.hpp"
using namespace std;

bool ehVizinho(string a, string b){
    int match=0;
    vector<int>mismatchId;
    int i=0, j=0; //i = Iterador da string "a" | j = Iterador da string "b"

    if (b.length() > a.length()){
        //Garante a > b sempre
        string aux;
        aux = a;
        a = b;
        b = aux;
    }
    
    while(i < a.length() && j < b.length()){
        if (a[i] == b[j]){
            match++;
            i++;
            j++;
        }
        else{
            mismatchId.push_back(i);
            i++;
            if (a.length() == b.length()){
                //Se as strings tem o mesmo tamanho, avança com os dois iteradores
                j++;
            }
        }
    }

    if (a.length() == b.length()){
        if (match == a.length()-1){
            //Substituição de uma letra
            return true;
        }
        else if (match == a.length()-2){
            //Troca de letras de uma mesma palavra
            string comp;
            int id1=mismatchId[0], id2=mismatchId[1];
            comp = a.substr(0, id1)+a[id2]+a.substr(id1+1, id2-id1-1)+a[id1]+a.substr(id2+1); //Faz a troca de posição das letras
            if (comp == b)
                return true;
        }
    }
    else if (match == a.length()-1){
        //Remoção de uma letra
        return true;
    }

    return false;
}

string getOnlyWord(string input){
    int len;
    //Remove do início
    for(len=0; isSymbol(input[len]); len++);
    input.erase(input.begin(), input.begin()+len);
    //Remove do fim
    for (len=input.length()-1; isSymbol(input[len]); len--);
    if (len != (int)(input.length()-1)){
        input.erase(input.begin()+(len+1), input.end());
    }
    return input;
}

bool isSymbol(char input){
    bool ret;
    switch (input){
    case ' ':
    case '_':
    case '\'':
    case '"':
    case '-':
    case '~':
    case '^':
    case '!':
    case '@':
    case '#':
    case '$':
    case '%':
    case '&':
    case '*':
    case '(':
    case ')':
    case '[':
    case ']':
    case '{':
    case '}':
    case '?':
    case '\\':
    case '/':
    case '.':
    case ',':
    case ':':
    case ';':
    case '<':
    case '>':
        ret = true;
        break;
    
    default:
        ret = false;
        break;
    }
    return ret;
}