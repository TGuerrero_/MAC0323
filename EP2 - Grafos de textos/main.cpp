#include "grafo.hpp"
#include <iomanip>
using namespace std;

/*-----------------------------------------------------------*/
/* Protótipo das funções*/
static void mostraUso(char *nomePrograma);
static void mostraOperacoes();
/* Protótipo testes automatizados */
void analiseDeMedidas(grafo &grafo);
void analiseDeConexidade(grafo &grafo);
void analiseDeCaminhos(grafo &grafo);
/* Protótipo testes  Manuais */
void testeInterativo(grafo &grafo, int k);
void analiseEntreVertices(grafo &grafo, string a, string b);

int main(int argc,char *argv[]){
    string nomeDoArquivo;
    if (argc < 3)
        mostraUso(argv[0]);

    nomeDoArquivo = argv[1];
    grafo teste(nomeDoArquivo, atoi(argv[2]));
    analiseDeMedidas(teste);
    analiseDeConexidade(teste);
    analiseDeCaminhos(teste);
    testeInterativo(teste, atoi(argv[2]));
    return 0;
}

static void mostraUso(char *nomePrograma){
    cout << "Uso \n"
        << "prompt> " << nomePrograma << " nome-arquivo parametro-k\n"
        << "    nome-arquivo = Nome do arquivo com o texto\n"
        << "    parametro-k  = Valor do parâmetro usado na construção do grafo\n";
    exit(EXIT_FAILURE);
}

static void mostraOperacoes(){
    cout<<"\n########## Teste Iterativo ##########\n";
    cout<<"Possíveis opções: analisaVtcs <word1> <word2>, cicloVtc <word>, cicloVtcs <word1> <word2>, tamComp <word>, ";
    cout<<"printAdjList, printVtcList<word>, adicionaVtc<word> \n";
    cout<<" >>> ";
}

void testeInterativo(grafo &grafo, int k){
    string op, word1, word2;
    mostraOperacoes();
    while (cin>>op){
        if (op == "analisaVtcs"){
            cin>>word1;
            cin>>word2;
            analiseEntreVertices(grafo, word1, word2);
        }
        else if (op == "cicloVtc"){
            cin>>word1;
            cout<<"A palavra '"<<word1<<"' aparece em algum ciclo: "<<boolalpha<<grafo.emCiclo(word1)<<endl;
        }
        else if (op == "cicloVtcs"){
            cin>>word1;
            cin>>word2;
            cout<<"As palavras aparecem em algum ciclo juntas: "<<boolalpha<<grafo.emCiclo(word1, word2)<<endl;
        }
        else if (op == "tamComp"){
            int tamComp;
            cin>>word1;
            tamComp = grafo.tamComp(word1);
            if (tamComp != -1)
                cout<<"A componente conexa que contém a palavra '"<<word1<<"' tem tamanho "<<tamComp<<endl;
            else
                cout<<"A palavra não está no grafo!"<<endl;
        }
        else if (op =="printAdjList"){
            grafo.printAdjList();
        }
        else if (op == "printVtcList"){
            cin>>word1;
            cout<<"Lista de adjacência da palavra '"<<word1<<"':"<<endl;
            grafo.printVertexAdjList(word1);
        }
        else if (op == "adicionaVtc"){
            cin>>word1;
            grafo.insere(word1, k);
        }
        else{
            cout<<"ERROR: Operação inválida!"<<endl;
        }
        mostraOperacoes();
    }
}

void analiseEntreVertices(grafo &grafo, string a, string b){
    int dist = grafo.dist(a, b);
    cout<<"\n### Análise entre Vértices ###\n";
    if (dist != -1)
        cout<<"A menor distância entre as palavras é: "<<dist<<endl;
    else
        cout<<"Não existe caminho entre as duas palavras"<<endl;
        
    cout<<"A palavra '"<<a<<"' aparece em algum ciclo: "<<boolalpha<<grafo.emCiclo(a)<<endl;
    cout<<"A palavra '"<<b<<"' aparece em algum ciclo: "<<boolalpha<<grafo.emCiclo(b)<<endl;
    cout<<"As palavras aparecem em algum ciclo juntas: "<<boolalpha<<grafo.emCiclo(a, b)<<endl;
}

void analiseDeMedidas(grafo &grafo){
    int vertices, arestas;
    vertices = grafo.vertices();
    arestas = grafo.arestas();
    cout<<"\n### Análise de Medidas ###\n";
    cout<<"Vértices: "<<vertices<<endl;
    cout<<"Arestas diferentes: "<<arestas<<endl;
    cout<<"Grau médio dos vértices: "<<fixed<<setprecision(2)<<((double)2*arestas)/vertices<<endl;
    if ( ((double)2*arestas)/(vertices*vertices) >= (1/4) && ((double)2*arestas)/(vertices*vertices) < 1)
        cout<<"Grafo denso: "<<boolalpha<<true<<endl;
    else
        cout<<"Grafo denso: "<<boolalpha<<false<<endl;
}

void analiseDeConexidade(grafo &grafo){
    vector<int>dados = grafo.dadosAnaliticosComp();
    cout<<"\n### Análise de Conexidade ###\n";
    cout<<"Grafo conexo: "<<boolalpha<<grafo.conexo()<<endl;
    cout<<"Componentes conexas: "<<grafo.componentes()<<endl;
    cout<<"Tamanho médio das componentes conexas: "<<dados[2]<<endl;
    cout<<"Tamanho da menor componente: "<<dados[0]<<endl;
    cout<<"Tamanho da maior componente: "<<dados[1]<<endl;
}

void analiseDeCaminhos(grafo &grafo){
    cout<<"\n### Análise de Caminhos ###\n";
    cout<<fixed<<setprecision(2);
    cout<<"A distância média entre dois vértices é: "<<grafo.distMedia()<<endl;
}
