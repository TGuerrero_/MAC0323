#include <fstream>
#include "grafo.hpp"
using namespace std;


/* Funções Privadas */

void grafo::dfs(int v, vector<bool>& marked){
    marked[v]=true;
    cel* aux=vertexList[v]->getNext();
    while (aux != nullptr){
        if (!marked[aux->getVertexId()])
            dfs(aux->getVertexId(), marked);
        aux = aux->getNext();
    }
}

/* Variação do algoritmo da dfs para achar a quantidade de
elementos em uma componente conexa */
int grafo::dfs_tamComp(int v, vector<bool>& marked){
    int compSize=1;
    cel*aux=vertexList[v]->getNext();
    marked[v]=true;

    while (aux != nullptr){
        if (!marked[aux->getVertexId()]){
            compSize += dfs_tamComp(aux->getVertexId(), marked);
        }
        aux = aux->getNext();
    }
    return compSize;
}

/* Recebe dois vértices vizinhos "cicloId" e "v" e checa se é possível
fazer o caminho v-cicloId sem passar pela aresta que liga os dois */
bool grafo::caminho_alternativo(int cicloId, int v){
    vector<int>dist(vertexList.size(), vertexList.size());
    queue<int>fila;
    dist[v]=0;
    fila.push(v);
    while (dist[cicloId] == vertexList.size() && !fila.empty()){
        int idAtual;
        cel*aux;
        idAtual = fila.front();
        fila.pop();
        aux = vertexList[idAtual]->getNext();
        while (aux != nullptr){
            if (dist[aux->getVertexId()] > dist[idAtual]+1 && ( idAtual != v || aux->getVertexId() != cicloId)){
                dist[aux->getVertexId()] = dist[idAtual]+1;
                fila.push(aux->getVertexId());
            }
            aux = aux->getNext();
        }
    }
    return dist[cicloId] != vertexList.size();
}

/* Recebe dois vértices vizinhos "cicloId" e "v" e checa se é possível fazer o caminho
v-cicloId sem passar pela aresta que liga os dois e passando pelo vértice "u" */
bool grafo::caminho_alternativo(int cicloId, int v, int u){
    vector<int>dist(vertexList.size(), vertexList.size());
    vector<int>pred(vertexList.size(), -1); //Vetor de predecessores 
    queue<int>fila;
    dist[v]=0;
    pred[v]=v;
    fila.push(v);
    while (dist[cicloId] == vertexList.size() && !fila.empty()){
        int idAtual;
        cel*aux;
        idAtual = fila.front();
        fila.pop();
        aux = vertexList[idAtual]->getNext();
        while (aux != nullptr){
            if (dist[aux->getVertexId()] > dist[idAtual]+1 && ( idAtual != v || aux->getVertexId() != cicloId)){
                pred[aux->getVertexId()] = idAtual;
                dist[aux->getVertexId()] = dist[idAtual]+1;
                fila.push(aux->getVertexId());
            }
            aux = aux->getNext();
        }
    }
    
    if (dist[cicloId] != vertexList.size()){
        //Checa se o vértice "u" faz parte do caminho
        int i = pred[cicloId];
        while (pred[i] != u && pred[i] != v){
            i = pred[i];
        }
        if (pred[i] == u)
            return true;
    }
    return false;
}



/* Funções Publicas */

/* Inicializa um grafo com parâmetro k */
grafo::grafo(string nomeDoArquivo, int k){
    fstream file;
    string fileName="./inputs/", word;
    fileName += nomeDoArquivo;
    file.open(fileName);
    if (file.fail()){
        cout<<"ERROR: Não foi possível ler o arquivo!"<<endl;
        exit(EXIT_FAILURE);
    }
    while (file >> word){
        word = getOnlyWord(word);
        insere(word, k);
    }
    file.close();
}


grafo::~grafo(){
    cel *aux, *lixo;
    for (int i=0; i < vertexList.size(); i++){
        aux = vertexList[i];
        while (aux != nullptr){
            lixo = aux;
            aux = aux->getNext();
            delete lixo;        
        }
    }
}


/* Insere a palavra e retorna o número de arestas adicionados ao grafo,
retorna -1 se a palavra já está no grafo ou tem tamanho menor que k */
int grafo::insere(string word, int k){
    cel*newWord, *newAdjWord;
    int arestas=0;
    if (word.length() < k)
        return -1;

    for (int i=0; i < vertexList.size(); i++){
        if (vertexList[i]->getWord() == word)
            return -1;
    }
    newWord = new cel;
    newWord->setWord(word);
    vertexList.push_back(newWord);
    newWord->setVertexId(vertexList.size()-1);

    for (int i=0; i < vertexList.size()-1; i++){
        if (ehVizinho(vertexList[i]->getWord(), word)){
            //Insere "word" na lista de adjacência de "vertexList[i]"
            newAdjWord = new cel;
            newAdjWord->setWord(word);
            newAdjWord->setNext(vertexList[i]->getNext());
            newAdjWord->setVertexId(newWord->getVertexId());
            vertexList[i]->setNext(newAdjWord);
            //Insere "vertexList[i]"" na lista de adjacência de "word"
            newAdjWord = new cel;
            newAdjWord->setWord(vertexList[i]->getWord());
            newAdjWord->setNext(vertexList[newWord->getVertexId()]->getNext());
            newAdjWord->setVertexId(i);
            vertexList[newWord->getVertexId()]->setNext(newAdjWord);

            arestas++;
            //Incrementa o tamanho das listas
            vertexList[i]->setAdjListSize(vertexList[i]->getAdjListSize()+1);
            vertexList[newWord->getVertexId()]->setAdjListSize(vertexList[newWord->getVertexId()]->getAdjListSize()+1);
        }
    }
    return arestas;
}


/* Retorna o número de vértices do grafo */
int grafo::vertices(){
    return vertexList.size();
}


/* Retorna o número de arestas do grafo */
int grafo::arestas(){
    int qtd=0;
    for (int i=0; i < vertexList.size(); i++){
        qtd += vertexList[i]->getAdjListSize();
    }
    return qtd/2;
}


/* Retorna o número de componentes do grafo */
int grafo::componentes(){
    int qtd=0;
    vector<bool>marked(vertexList.size(), false);
    for (int i=0; i < vertexList.size(); i++)
        if (!marked[i]){
            qtd++;
            dfs(i, marked);
        }
    return qtd;
}


/* Retorna se o grafo é ou não conexo */
bool grafo::conexo(){
    if (componentes() == 1)
        return true;
    return false;
}


/* Retorna o tamanha da componente conexa onde está a palavra
ou -1 caso ela não se encontre no grafo */
int grafo::tamComp(string word){
    int wordId=-1;
    vector<bool>marked(vertexList.size(), false);
    for (int i=0; wordId == -1 && i < vertexList.size(); i++)
        if (vertexList[i]->getWord() == word)
            wordId = i;
    
    if (wordId == -1)
        return -1;

    return dfs_tamComp(wordId, marked);
}


/* Retorna a menor distância entre as palavras a e b ou -1
caso elas estejam desconexas ou não estejam no grafo */
int grafo::dist(string a, string b){
    vector<int>dist(vertexList.size(), vertexList.size());
    queue<int>fila;
    int id1=-1, id2=-1;
    for (int i=0; (id1 == -1 || id2 == -1) && i < vertexList.size(); i++){
        //Descobre o indice do vértice que contém a string "a" e o indice que contém a string "b"
        if (vertexList[i]->getWord() == a)
            id1 = i;
        else if (vertexList[i]->getWord() == b)
            id2 = i;
    }
    if (id1 == -1 || id2 == -1)
        return -1;

    fila.push(id1);
    dist[id1] = 0;
    while (dist[id2] == vertexList.size() && !fila.empty()){
        int idAtual;
        cel* aux;
        idAtual = fila.front();
        fila.pop(); 
        aux = vertexList[idAtual]->getNext();
        while (aux != nullptr){
            if (dist[aux->getVertexId()] > dist[idAtual]+1){
                dist[aux->getVertexId()] = dist[idAtual]+1;
                fila.push(aux->getVertexId());
            }
            aux = aux->getNext();
        }
    }
    if (dist[id2] == vertexList.size())
        return -1;
    return dist[id2];
}


/* Retorna verdadeiro casa a palavra esteja em algum ciclo,
falso caso contrário */
bool grafo::emCiclo(string a){
    int wordId=-1;
    cel*aux;
    for (int i=0; wordId == -1 && i < vertexList.size(); i++)
        if (vertexList[i]->getWord() == a)
            wordId = i;
    
    if (wordId == -1 || vertexList[wordId]->getAdjListSize() < 2)
        return false;
    
    aux = vertexList[wordId]->getNext();
    while (aux != nullptr){
        if (caminho_alternativo(wordId, aux->getVertexId()))
            return true;
        aux = aux->getNext();
    }
    return false;
}


/* Retorna verdadeiro casa exista um ciclo que contenha ambas as palavras,
falso caso contrário */
bool grafo::emCiclo(string a, string b){
    int wordId1=-1, wordId2=-1;
    cel*aux;
    for (int i=0; (wordId1 == -1 || wordId2 == -1) && i < vertexList.size(); i++){
        if (vertexList[i]->getWord() == a)
            wordId1 = i;
        else if (vertexList[i]->getWord() == b)
            wordId2 = i;
    }
    
    if (wordId1 == -1 || wordId2 == -1 || vertexList[wordId1]->getAdjListSize() < 2)
        return false;
    
    aux = vertexList[wordId1]->getNext();
    while (aux != nullptr){
        if (caminho_alternativo(wordId1, aux->getVertexId(), wordId2))
            return true;
        aux = aux->getNext();
    }
    return false;
}


/* Através de uma dfs, retorna um vetor t.q:
 - vector[0] == Tamanho da menor componente conexa;
 - vector[1] == Tamanho da maior componente conexa;
 - vector[2] == Tamanho médio das componentes conexas. */
vector<int> grafo::dadosAnaliticosComp(){
    vector<int>dadosComps(3, 0);
    vector<bool>marked(vertexList.size(), false);
    int qtd=0, atual;
    for (int i=0; i < vertexList.size(); i++)
        if (!marked[i]){
            qtd++;
            atual = dfs_tamComp(i, marked);
            if (atual > dadosComps[1])
                dadosComps[1] = atual;
            if (atual < dadosComps[0] || dadosComps[0] == 0)
                dadosComps[0] = atual;
            dadosComps[2] += atual;
        }
    dadosComps[2] = dadosComps[2]/qtd;
    return dadosComps;
}


/* Retorna a distância mínima média entre
quaisquer dois vértices do grafo */
double grafo::distMedia(){
    queue<int>fila;
    int distCount=0, caminhos=0;
    for (int i=0; i < vertexList.size(); i++){
        vector<int>dist(vertexList.size(), vertexList.size());
        dist[i] = 0;
        fila.push(i);
        while (!fila.empty()){
            int atual;
            cel* aux;
            atual = fila.front();
            fila.pop();
            aux = vertexList[atual]->getNext();
            while (aux != nullptr){
                if (dist[aux->getVertexId()] > dist[atual]+1){
                    dist[aux->getVertexId()] = dist[atual]+1;
                    distCount += dist[atual]+1;
                    caminhos++;
                    fila.push(aux->getVertexId());
                }
                aux = aux->getNext();
            }
        }
    }
    return ((double)distCount/caminhos);
}


/* Printa toda a lista de adjacência da palavra "word" */
void grafo::printVertexAdjList(string word){
    int wordId=-1;
    cel* aux;
    for (int i=0; wordId == -1 && i < vertexList.size(); i++){
        if (vertexList[i]->getWord() == word)
            wordId = i;
    }
    if (wordId == -1)
        return;
    aux = vertexList[wordId];
    while(aux != nullptr){
        cout<<aux->getWord();
        if (aux->getNext() != nullptr)
            cout<<" -> ";
        aux = aux->getNext();
    }
    cout<<endl;
}


/* Printa toda a lista de vértices e, para cada vértice, printa a lista de adjacência */
void grafo::printAdjList(){
    for (int i=0; i < vertexList.size(); i++){
        printVertexAdjList(vertexList[i]->getWord());
    }
}