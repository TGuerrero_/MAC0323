#include <iostream>
#include <queue>
#include "cel.hpp"
#include "util.hpp"
using namespace std;

#ifndef GRAFO_H
#define GRAFO_H

class grafo{
    private:
        vector<cel*>vertexList;
        void dfs(int v, vector<bool>& marked);
        int dfs_tamComp(int v, vector<bool>& marked);
        bool caminho_alternativo(int cicloId, int v);
        bool caminho_alternativo(int cicloId, int v, int u);
    public:
        grafo(string nomeDoArquivo, int k);
        ~grafo();
        int insere(string word, int k);
        int vertices();
        int arestas();
        int componentes();
        bool conexo();
        int tamComp(string word);
        int dist(string a, string b);
        bool emCiclo(string a);
        bool emCiclo(string a, string b);
        vector<int> dadosAnaliticosComp();
        double distMedia();
        void printVertexAdjList(string word);
        void printAdjList();
};
#endif