#include <iostream>
using namespace std;

#ifndef CEL_H
#define CEL_H

class cel{
    private:
        string word;
        cel *next; 
        int adjListSize;
        int vertexId;
    public:
        cel(): next(nullptr), adjListSize(0), vertexId(-1){};
        string getWord();
        cel* getNext();
        int getAdjListSize();
        int getVertexId();
        void setWord(string newWord);
        void setNext(cel *newNext);
        void setAdjListSize(int newSize);
        void setVertexId(int newId);
};
#endif