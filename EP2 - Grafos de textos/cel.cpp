#include "cel.hpp"
using namespace std;

string cel::getWord(){
    return word;
}

cel* cel::getNext(){
    return next;
}

int cel::getAdjListSize(){
    return adjListSize;
}

int cel::getVertexId(){
    return vertexId;
}

void cel::setWord(string newWord){
    word = newWord;
}

void cel::setNext(cel *newNext){
    next = newNext;
}

void cel::setAdjListSize(int newSize){
    adjListSize = newSize;
}

void cel::setVertexId(int newId){
    vertexId = newId;
}