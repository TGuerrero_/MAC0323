#include<iostream>
#include<vector>
using namespace std;
#ifndef UTIL_H
#define UTIL_H

/* 
* Checa se a string "a" é vizinha da string "b" a partir das
* definições dadas no enunciado
*/
bool ehVizinho(string a, string b);

/*
* getOnlyWord()
* Recebe uma string "input" e remove todos os simbolos
* que não são letras do início e do fim da palavra.
* Retona a nova string.
*/
string getOnlyWord(string input);

/*
* isSymbol()
* Retorna true se o "input" é um simbolo e não uma letra.
*/
bool isSymbol(char input);
#endif